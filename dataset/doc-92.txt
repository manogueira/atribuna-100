
Pol í t i ca
20 ATRIBUNA VITÓRIA, ES, SEGUNDA-FEIRA, 02 DE JANEIRO DE 2017

FALE COM O EDITOR GLEBERSON NASCIMENTO E-MAIL: politica@redetribuna.com.br

POSSES NA GRANDE VITÓRIA

Bate-boca e pancadaria na Serra
Por causa do tumulto
na Câmara, Audifax
esperou três horas
para conseguir tomar
posse como chefe
do Executivo

FOTOS: KADIDJA FERNANDESAT

VEREADORA
Cleusa Paixão
(PMN) discute
com apoiadores
de Neidia
Pimentel (PSD)
durante eleição
conturbada
para a Mesa
Diretora. A
vereadora saiu
em defesa da
filha, que se
envolveu em um
tumulto dentro
do plenário
enquanto a mãe
discursava da
tribuna contra
a presidente
reconduzida ao
cargo pela base
do prefeito

Um cenário de tumulto, bate-boca e agressões marcou a primeira sessão da Câmara
de Vereadores da Serra em 2017,
que definiu a Mesa Diretora da
Casa pelos próximos dois anos.

Diante de uma reviravolta que
reconduziu a vereadora Neidia Pi-
mentel (PSD) à presidência da Ca-
sa – por meio da base de apoio ao
prefeito Audifax Barcelos (Rede)
–, vereadores da oposição chega-
ram a esvaziar a sessão e pessoas
presentes no plenário foram reti-
radas do ambiente pela polícia.

A confusão começou após ser li-
da a inscrição das chapas para dis-
puta da Mesa em que Neidia ocu-
pava o cargo de primeira-secretá-
ria do chamado “Grupo dos 12” –
de oposição ao prefeito – e de pre-
sidente na chapa oposta, apoiada
pela base aliada.

Questionada sobre qual chapa
pretendia compor, Neidia esco-
lheu ser candidata a presidente, o
que revoltou o filho do vereador
Nacib Haddad (PDT), articulador
da oposição, presente no plenário.
Alterado, Samir Haddad atirou pa-
péis em direção ao vereador
Adriano Galinhão (PTC), que pre-
sidia a sessão. O fato gerou tumulto
generalizado, agressões, e a Polícia
Militar foi chamada para retirar
Samir da sessão, que foi suspensa.

Com 23 vereadores na Casa, a
mudança de Neidia virou o jogo
pela presidência e deixou mais
forte o grupo em apoio ao prefeito,
agora com 12 membros.

Durante o período em que este-
ve suspensa a sessão, a oposição
chegou a articular a entrada de
Luiz Carlos Moreira (PMDB) co-
mo presidente em uma nova cha-
pa, o que não chegou a ser concre-
tizado. Sem um dos integrantes
necessários, a Chapa 1 acabou reti-
rada da disputa, o que gerou mais
revolta entre seus membros.

Candidato a presidente pela
chapa dissolvida, Basílio da Saúde
(Pros) agrediu verbalmente Nei-
dia, a quem chamou de “sem cará-
ter ” na tribuna da Câmara. O ve-
reador chegou a jogar um extintor
de incêndio na direção da colega,
mas não a atingiu.

Após a sessão que deu posse a
Neidia com um placar de 11 votos a
zero, Basílio se disse arrependido e
pediu desculpas pelas agressões,
que não foram únicas. Além dele,
outros membros da Casa e convi-
dados se dirigiram à vereadora co-
mo sendo “t ra i d o ra ” da oposição.

Apoiado pela nova presidente,
Audifax não comentou sobre a dis-
puta pela Mesa. “Preferia não estar
aqui”, disse ele enquanto esperava
sua posse, que ocorreu três horas
depois do previsto.

Pre s i d e n t e
reeleita da
Câmara diz
ter sido traída

Presidente reeleita da Câmara,
Neidia Pimentel (PSD) se disse
traída pelo PDT, após a confusão e
os xingamentos a ela direcionados.

Segundo Neidia Pimentel, a con-
dição para o PSD apoiar o candida-
to derrotado a prefeito, Sérgio Vi-
digal (PDT), nas eleições era o PDT
apoiar a recondução da vereadora
à presidência da Casa, o que não te-
ria acontecido.

“Não mudei de lado. Havia uma
composição partidária. Não traí e
fui traída”, disse a vereadora, que
se inscreveu de última hora para a
presidência pela chapa do prefeito
Audifax Barcelos (Rede).

Procurado para comentar o as-
sunto, Sérgio Vidigal (PDT) disse
ter recebido a fala “com surpresa” e
que, como deputado federal, não
interfere nos processos de votação
da Câmara Municipal.

MESA DIRETORA

Se r ra
> P R ES I  D E N T E :  Neidia Pimentel

( P S D)
> 1º VICE-PRESIDENTE: Rodrigo Cal-

deira (Rede)
> 2º VICE-PRESIDENTE: Robson Mi-

randa, o “Robinho Gari” (PV )
> 1º SECRETÁRIO: Roberto Ferreira da

Silva (PHS)
> 2º SECRETÁRIO: Adriano Vasconce-

los, o “Adriano Galinhão” (PTC )

CENAS DA POSSE NA SERRA

OS VEREADORES Basílio da Saúde (Pros), Neidia Pimentel
(PSD) e Guto Lorenzoni (PP) discutem durante a sessão

A CONFUSÃO foi generalizada após o início do processo de
eleição da Mesa Diretora. Houve empurra-empurra.

SA M I R Haddad (camisa rosa), filho do vereador Nacib Had-
dad (PDT), é contido durante a confusão

COM A GALERIA lotada, os cidadãos acompanharam tudo de
perto com direito a buzinas e socos no vidro de contenção

Líder da
oposição diz
que vai entrar
na Justiça

Líder informal da oposição ao
prefeito na Câmara, o vereador
Nacib Haddad (PDT) prometeu
levar a disputa pela Mesa Diretora
à Justiça.

De acordo com o vereador, que é
dono do sítio Manancial, onde a
oposição se reuniu por quase um
mês para definir sua chapa, Neidia
Pimentel (PSD) tentou ser candi-
data à presidência pelo grupo, mas
não conseguiu apoio.

“Ela tentou, mas não conseguiu
apoio para ser candidata à presi-
dência. Mas isso não justifica estar
com o nome inscrito em duas cha-
pas. Não tem ninguém no primário
aqui e temos que ter compromisso
com documentos assinados”, disse
o vereador, antes de concluir:

“A votação foi feita de maneira
completamente irregular e desres-
peitando o regimento interno da
Câmara. Não há possibilidade de
não entrarmos na Justiça”.

P R ES I D E N T E
eleita na Câ-
mara da Ser-
ra, a vereado-
ra Neidia Pi-
mentel (PSD)
chora duran-
te a votação
da Mesa


