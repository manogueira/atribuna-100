
Ci d a d e s

6 ATRIBUNA VITÓRIA, ES, SEGUNDA-FEIRA, 02 DE JANEIRO DE 2017

VERÃO

Point da balada nas
areias da Praia da Costa
Diversão ao som de
música eletrônica
acontece sempre aos
sábados e domingos e
até em dias de semana,
durante o verão

FOTOS: ANTONIO COSME/AT

ANIMAÇÃO NO POSTO 9

Bairro recebe
visitas de
“A Tribuna
com Você”

Nesta semana, “A Tribuna com
Vo c ê ” vai estar na Praia da Costa,
em Vila Velha, que possui cerca de
31 mil habitantes. A última vez que
o projeto esteve no bairro foi em
2 0 1 5.

O objetivo da visita é mostrar as
potencialidades do bairro, dando
destaque para comércio e talentos.
Ainda, contar casos curiosos e co-
nhecer a história da região.

As reportagens serão publicadas
a partir de amanhã até sexta-feira
e serão mostradas também as ne-
cessidades e reivindicações da po-
p u l a ç ã o.

Durante toda a semana a repor-
tagem de A Tribuna percorrerá as
principais ruas do bairro para fa-
zer contato com os moradores. O
objetivo é receber sugestões, dicas
de histórias, perguntas e reivindi-
c a ç õ e s.

Não vai faltar espaço para os que
são talentosos. Esportistas, arte-
sãos de sucesso, músicos que en-
cantam a vizinhança e aqueles que
têm habilidades na confeitaria ou
com a costura.

Os questionamentos e as reivin-
dicações dos moradores da Praia
da Costa vão ser respondidos pela
prefeitura e publicadas na edição
de quinta-feira.

Os moradores que quiserem en-
viar sugestões de reportagem po-
dem escrever uma mensagem com
nome e telefone para contato e en-
viá-la para o e-mail atcomvo-
c e @ re d e t r i b u n a .c o m . b r.

Moradores de outros bairros
também podem sugerir uma visita
do projeto pelo e-mail, basta en-
viar o nome do bairro e as mesmas
informações de contato.

BANHISTAS na Praia da Costa

Luciana Almeida

Um lugar para apreciar belezas naturais, gente bonita, praticar esportes e até para
curtir uma balada. Tudo na areia
da Praia da Costa, em Vila Velha.

No local, o Posto 9, como ficou
conhecido o trecho próximo à rua
Castelo Branco, transformou-se
em point para jovens sarados, de
bem com a vida, que curtem músi-
ca eletrônica e a prática de espor-
tes, e já é conhecido por diversos

grupos de amigos.
Lá, muitas pessoas que querem

curtir o dia chegam, montam suas
barracas e, de posse de caixas de
som, colocam as batidas que mais
fazem sucesso nas baladas.

Não há tendas ou mesmo lâmpa-
das coloridas para atrair a atenção
das pessoas, mas é uma faixa de
areia que ficou conhecida pela
presença desses grupos de jovens.

O movimento de música eletrô-
nica acontece sempre no sábado
depois do meio-dia, e também no
domingo pela manhã, mas em dias
de semana durante o verão a bala-
da no local também é garantida.
Uma espécie de boate na areia, co-
mo muitos frequentadores da re-
gião dizem ser o local.

A recepcionista Ana Paula Rivoli,
21 anos, contou que muitos fre-
quentadores do Posto 9 vão no sá-
bado à tarde para aquecer para as

baladas noturnas. “Sempre tem
música eletrônica e fica parecendo
uma boate. A galera vem aquecer
para, à noite, ir para a boate. Isso
aqui sempre bomba”, afirmou.

A jovem disse ainda que também
é comum as pessoas saírem das ba-
ladas no início da manhã de do-
mingo e esticarem a festa para a
praia, levando novamente a músi-
ca eletrônica para as areias da
Praia da Costa. “Domingo pela
manhã a gente chama de ressaca
do rock”, completou.

Ela e os amigos Diogo Lourenço,
30, que é músico, Leydis Raiane
Oliveira, 24, e Eduardo Cavalieri,
26, ambos universitários, são fre-
quentadores assíduos do local.

“Até mesmo outras pessoas que
a gente não conhece colocam as
cadeiras mais perto da gente ou de
quem estiver com a caixinha de
som para curtir a música”, disse
Leydis Raiane.

ES P O RT ES
Além das batidas da música ele-

trônica, muitos jovens são atraídos
para o local para praticar esportes.

O universitário Leonardo Cas-
tro, 24, contou que fica mais diver-
tido jogar vôlei em um local emba-
lado por música.

“Anima mais e fica mais diverti-
do. A gente frequenta o local, pra-
tica esportes, ouve boa música e
faz novos amigos”, disse.

“Sempre tem música eletrônica e fica parecendo uma boate. Isso aqui bomba” Ana Paula Rivoli, recepcionista

OS AMIGOS Leydis Oliveira, Eduardo Cavalieri, Diogo Lourenço e Ana Paula Rivoli costumam frequentar o local para curtir música na areia da praia

Passeio em Vila Velha
O casal Lucas Lira, de 26 anos, e

Monique Bolsoni, 20, é da cidade de
São Gabriel da Palha, no Noroeste do
Estado, e comemorou a chegada de
2017 em Vila Velha.

Eles se hospedaram na casa do ir-
mão de Lucas, o autônomo Tiago Li-
ra Azevedo, 22, que mora no municí-
pio, e os três optam por frequentar a
área do Posto 9 da Praia da Costa por
conta da música e por ser um local
animado, com muita gente jovem.

“Nesse local sempre tem muita
gente com o astral muito bom”, disse
Lucas.

Jogo de vôlei
O casal de namorados Sarah Cas-

tro, 19 anos, e Johnny Rangel, 21, sem-
pre se reúne com os amigos Rodolfo
Maia, 22, e Kevlyn Figueiredo, 21, para
uma partida de vôlei no Posto 9, na
Praia da Costa, em Vila Velha.

Johnny contou que a área está
sempre repleta de jovens que costu-
mam frequentar o local para jogar vô-
lei ou futebol. Já Rodolfo lembrou que
há várias barracas que colocam músi-
ca eletrônica, e isso também é um
at rat i vo .


