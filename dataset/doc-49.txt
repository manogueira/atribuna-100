
Ec o n o m i a

24 ATRIBUNA VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017

OPINIÃO
ECO N Ô M I CA
SAMUEL PESSÔA

Olhando para trás
e para a frente
Na coluna de 10 de janeiro do ano passado, anotei que a hipótese básica de meu cenário para 2016 era que ele dependeria pouco de haver ou não troca de Dilma por Temer. Considerava que nossos pro-
blemas estruturais eram graves demais para serem encaminhados rapida-
mente. Projetei que o crescimento em 2016 seria negativo em 3%, com con-
tinuidade do ajuste externo e déficit de transações correntes de US$ 35 bi-
lhões e com inflação de 7,5%. O câmbio de final de ano seria de R$ 4,6 por
d ó l a r.

A troca de governo mudou as
expectativas, e o câmbio se valori-
zou. Em vez dos R$ 4,6, termina-
mos o ano com câmbio na casa de
R$ 3,2.

O câmbio 30% mais valorizado
do que o previsto tirou 0,81 ponto
percentual da inflação, sugerindo
IPCA de 6,7%, em vez dos 7,5%
p ro j e t a d o s.

Boa parte do erro de previsão
da inflação – 7,5%, no lugar dos
6,3% com que o ano deve fechar –
deve-se ao erro no cenário de
c â m b i o.

O ano se encerrará com déficit
externo US$ 18 bilhões abaixo do
previsto. A queda mais acentuada
do investimento, que resultou
num cenário de atividade pior do
que o previsto (-3,5%, em vez de
-3%), explica parte da diferença,
mas não toda. A velocidade do
ajustamento externo surpreen-
deu.

A grande incógnita em 2017 se-
rá a tramitação do projeto de re-
forma da Previdência. Se o gover-
no conseguir aprovar tudo que
enviou, teremos ao final de 2017
um País muito melhor.

O desequilíbrio das contas pre-
videnciárias é de longe nosso
maior problema fiscal. Se houver
essa surpresa positiva, haverá no-
va rodada de valorização do câm-
bio e aceleração dos investimen-
tos, com crescimento mais forte
da economia no segundo semes-
tre e um ótimo carregamento es-
tatístico dessa expansão para
2 0 1 8.

Se Temer conseguir aprovar
uns 3/4 do que enviou da reforma
da Previdência, o câmbio no final
de 2017 será de aproximadamen-
te R$ 3,4 por dólar, com cresci-
mento do PIB de 0,3% na média
de 2017 ante a média de 2016.

No último trimestre de 2017,
ante o trimestre imediatamente
anterior, a economia estará ro-
dando a 2,7% ao ano. O carrega-
mento para 2018 será bom, mas
não ótimo.

Finalmente, se Temer não con-
seguir aprovar a reforma da Pre-

vidência, ou aprovar versão mui-
to diluída, teremos nova rodada
de depreciação da moeda e maior
lentidão na retomada da econo-
mia. Poderemos ter crescimento
negativo em 2017.

Considero mais provável o ce-
nário de que Temer aprove 3/4,

aproximadamente, da reforma da
P rev i d ê n c i a .

O crescimento econômico será
de 0,3%, a inflação, de 5%, e a Se-
lic no final de ano estará na casa
de 10,5%, com câmbio por volta
de R$ 3,4 por dólar.

Esse é o cenário central, contin-
gente a um sucesso bastante satis-
fatório, mesmo que não total, na
tramitação da reforma da Previ-
dência.

Meu relativo otimismo com re-

lação à aprovação da reforma da
Previdência deve-se a dois moti-
vos. Primeiro, há claros sinais de
que Temer tem coordenado bas-
tante bem sua base de sustenta-
ção. Segundo, a aprovação da re-
forma também interessa à es-
q u e rd a .

Se a reforma for aprovada, e a
esquerda ganhar em 2018, seja
com Lula ou com Ciro Gomes, co-
lherá, como ocorreu com Lula, os
efeitos benéficos das reformas es-
truturais feitas pelo grupo políti-
co adversário.

A todos, um ótimo 2017.

Se Temer aprovar uns 3/4 da reforma da
Previdência, o PIB crescerá 0,3% em 2017

e o dólar ficará em R$ 3,4

Publicação simultânea com a Folha de São Paulo AMANHÃ, NESTA COLUNA, Marcia Dessen

SAMUEL PESSÔA é doutor
em economia e pesquisador
associado do Instituto Brasileiro
de Economia da FGV.

DIA A DIA
COM A COLABORAÇÃO DE RAFAEL GUZZO | diadia@redetribuna.com.br

* * *

* * *

* * *

C U RTAS

Mais 400 empregos em porto do Sul
O Porto de Gamboa, projetado para Itapemirim, vai criar 400

empregos a mais que os 800 previstos inicialmente: serão 1.200
vagas. Isso se deve à expansão do projeto, que agora inclui mo-
vimentação de cargas gerais, além do suporte às operações pe-
trolíferas. Serão de 4 a 6 berços, incluindo contêineres, voltados
para a nova finalidade, disse o deputado Theodorico Ferraço.

O porto é da C-Port, empresa do grupo americano Edison
Chouest, e será inicialmente do tamanho do de Florianópolis (SC),
mas com previsão de se tornar um terminal de águas profundas,
segundo Theodorico: “Na 2ª etapa, basta avançar 1.200 metros
em direção ao mar que se alcança a profundidade adequada.”

Conforme Dia a Dia revelou ontem, a empresa busca parceria
com empresas especializadas em logística, como Roterdã e Cinga-
pura, e o investimento pode ter parceria com a japonesa Mitsui.

A empresa já tem projeto pronto, tendo só de adaptar, além de
licenças ambientais e área acima de 1 milhão de m2 adquirida.

Audiência com Temer
Audiência entre representantes da

Edison Chouest com o presidente Temer
já é viabilizada, para apresentar o proje-
to do Porto de Gamboa, em Itapemirim.

“O governador Hartung recebeu bem
a proposta de movimentar cargas ge-
rais e, agora, a deputada Norma Ayub
(que toma posse na Câmara amanhã)
está em contato com a Presidência”,
disse o deputado Theodorico Ferraço.

Com nome de mulher
A catarinense Wanke, indústria de

eletrodomésticos que está em vias de
instalar uma fábrica em Linhares, tem
uma linha de máquinas de lavar rou-
pas com uma característica peculiar:
os modelos têm nomes de mulheres.

Isabela lava até 12 kg; Bárbara, 10 kg;
Mariana e Maria, 7 kg; e Lis, 4 kg. Já os
demais produtos, como fornos e ven-
tiladores, não seguem esse estilo.

Fé no desconto
para pagar
em dinheiro

Liberação do saldo de
contas  inat ivas do
FGTS, redução dos ju-
ros do cartão e descon-
to para pagar por com-
pras em dinheiro: as
medidas anunciadas
pelo governo federal
animaram o presidente
da Câmera Setorial da
Indústria Moveleira,
Luiz Rigoni. “Vai au-
mentar o consumo e
melhorar a economia”,
disse, ao lembrar que
várias empresas do se-
tor estão em apuros.

Nem hiperinflação foi tão ruim
“Há 30 anos sou gestor em órgãos públicos e

nem na época do confisco da poupança (gover-
no Collor) vi uma crise como esta no País. Aliás,
nem na era da hiperinflação (década de 1980).”
A declaração é do diretor da Itaoca Offshore,
Alvaro de Oliveira Júnior. “As empresas tiveram
de empurrar a despesa de 2016 para 2017. Nun-
ca havia visto isso, só em livros”, concluiu.

FGTS deveria ser liberado para todos, diz sindicalista
Nem a liberação do dinheiro em contas inativas do FGTS foi capaz de amenizar

a bronca dos sindicalistas com o governo Temer. Para o presidente do Sindicomer-
ciários, Jakson Andrade, todos deveriam ser liberados para sacar o saldo do fun-
do. Ele detonou, ainda, a minirreforma trabalhista proposta pelo Presidente: “Ess a
possibilidade de fazer hora extra e trabalhar 12 horas por dia é ruim, fracionamento
de férias é ruim... é um desastre! Um presente de grego para o trabalhador.”

CERVEJARIA NO ESTADO
A cervejaria que está nos planos de

empresários para a região Norte do Es-
tado nada tem de artesanal: é um gran-
de empreendimento, garantem fontes
de mercado. Três municípios são cota-
dos: Linhares, São Mateus e Jaguaré,
mas nada está definido ainda.

MAIS GASTO COM FAXINEIRA
O aumento do pó preto em Vitória

tem causado transtornos ao bolso e à

saúde: com o acúmulo de poeira, a boa
e velha faxina semanal não resolve
mais a sujeira. É preciso pagar uma dia-
rista pelo menos dois dias por semana
— o que aumentou a procura por pro-
fissionais —, ou pôr a mão na massa.

MUITA NOTÍCIA BOA...
...é o que a Coluna Dia a Dia deseja

continuar levando a seus leitores nes-
te ano. Que 2017 seja repleto de opor-
tunidades para todos!


