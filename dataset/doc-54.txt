
Ec o n o m i a

VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 29

Imposto maior
para energia,
gasolina e
chope no Rio
RIO

Depois de obter uma decisão
favorável no Supremo Tribunal
Federal (STF), o governador do
Rio, Luiz Fernando Pezão, san-
cionou a lei que aumenta alí-
quotas de ICMS para energia
elétrica, gasolina, telefonia, cer-
veja e chope no estado.

A aprovação do projeto na
Assembleia havia sido suspensa
pela Justiça do Rio, em primei-
ra e segunda instâncias, a partir
de ação do deputado Flávio
Bolsonaro (PSC).

O procurador-geral do esta-
do, Leonardo Espíndola, autor
do pedido do governo junto ao
STF, disse que, se a decisão não
saísse hoje, o Rio perderia R$ 1
bilhão em recursos.

“Essa era a grande questão da
ação. Se a presidente (do STF,
ministra Carmem Lúcia) tivesse
dado a liminar no dia primeiro
de janeiro, ela seria completa-
mente ineficaz para o estado co-
brar os tributos ao longo de 2017.
Perderíamos todo o ano de 2017
e R$ 1 bilhão. A lei só começará a
produzir efeitos em 90 dias. Foi
muito prudente a decisão.”

Pezão (PMDB) vetou inte-
gralmente o projeto de lei que
ele mesmo encaminhou à As-
sembleia para redução de 30%
do seu salário e de seus secretá-
rios. A proposta era parte do
pacote para o corte de gastos do
governo elaborado pelo gover-
nador em novembro.

O p e ra d o ra s
de telefonia
avisam que
conta vai subir
B R AS Í L I A

A conta do celular pós-pago
ou controle fica mais cara neste
ano. Decisão do STF, de outu-
bro, autoriza a cobrança de
ICMS sobre a assinatura men-
sal e, com isso, os planos vão su-
bir. No caso da Oi, a alta chega a
35%.

Um plano pós-pago que custa
R$ 21 por mês passará para
R$ 28,30, a partir de 1º de feve-
reiro. A TIM disse que não vai
falar sobre o assunto. A Claro
afirmou que já cobra o ICMS.

O reajuste começa a valer ho-
je na Vivo. No site da operado-
ra, é possível consultar, de acor-
do com o plano, o novo valor.

As operadoras, porém, desta-
cavam o plano básico do cálculo
do imposto. Em algumas fatu-
ras, é possível ver que os demais
pacotes têm incidência de im-
postos, mas não a assinatura bá-
sica.

O ministro Teori Zavascki
afirmou, na decisão, que a co-
brança de ICMS incide sobre a
tarifa “independentemente da
franquia de minutos concedida
ou não”.

Leilão suspenso não desanima setor
Apesar da perspectiva de forte

crescimento, o governo federal
cancelou o leilão de energia reno-
vável, que seria realizado em 19 de
dezembro e incluía projetos de
energia solar.

O presidente da Associação Bra-
sileira de Energia Solar Fotovoltai-
ca (Absolar), Rodrigo Sauaia, disse
que o setor, que se preparava para
participar do leilão, foi surpreendi-
do com a suspensão. Para o execu-
tivo, foi sinal ruim aos investidores.

O executivo calculou que pode-
riam ter sido contratados pelo me-
nos 1.500 megawatts (MW) em
projetos de energia solar, que re-
presentariam investimentos de
R$ 9 bilhões até 2019.

“Foi um golpe duro para o setor
no momento em que está em fase
de desenvolvimento. É um sinal
muito ruim para atrair novos in-
vestimentos, seja na ampliação da
geração ou na fabricação de equi-
pamentos. A energia solar está se
tornando fonte complementar de

RODRIGO SAUAIA disse que cancelamento do leilão foi um “golpe duro”

País dá a largada para o
“boom” da energia solar
Especialistas apontam
que a participação da
energia solar na matriz
energética vai passar
do 0,02% em 2015 para
mais de 10% em 2030

RIO

A geração de energia solar está experimentando um boom, como ocorreu anos
atrás com a energia eólica, tendo
crescido mais de 70% a capacida-
de nos últimos dois anos. Cerca de
90% das unidades existentes fo-
ram instaladas neste período, se-
gundo dados da SER Energia.

Com base nas projeções feitas
pela Empresa de Pesquisa Energé-
tica (EPE), os projetos de energia
solar podem chegar a 25 gigawatts
(GW) em potência instalada até
2030. Eles poderão representar in-
vestimentos de R$ 125 bilhões.

Com essa expansão, estima-se
que a participação da energia solar
na matriz energética vai passar de
0,02% em 2015 para mais de 10%
em 2030. A expectativa é de que
dos 25 GW de energia solar previs-
tos para serem instalados até 2030,
17 GW sejam de geração centrali-

zada (usinas de grande porte) e 8,2
GW de geração distribuída (em
casas, edifícios comerciais e públi-
cos, condomínios e na área rural).

Ao todo, existem 111 projetos em
andamento, dos quais 12 em cons-
trução e outros 99 que ainda não
foram iniciados. Com esses proje-
tos, a Absolar estima que, já em
2018, a participação da energia so-
lar ficará entre 2% e 3%.

De acordo com dados da Absolar,
o setor de microgeração solar cres-
ceu 320% em 2015, com mais de 6
mil sistemas em todo o País, com

42 MW em potência instalada, in-
vestimento de R$ 375 milhões.

Diretor executivo da SER Ener-
gia, Adley Piovessan disse que o
crescimento das fontes renováveis
na China e na Alemanha vem redu-
zindo os custos dos equipamentos.
“Essa redução de preços fez o pra-
zo de retorno do investimento cair
de 25 anos para oito anos”.

Ele afirmou ainda que o governo
tem de aumentar linhas de crédito
para compra de equipamentos no
exterior e reduzir impostos.

Cyro Boccuzzi, membro do Ins-

tituto dos Engenheiros Eletricistas
e Eletrônicos, diz que há avanços
na regulação sendo elaborada pela
Aneel, permitindo a chamada ge-
ração remota por microempreen-
dedores. O especialista estima que,
por R$ 20 mil, é possível instalar
unidade para gerar 2 KW, suficien-
te para suprir 90% do consumo de
uma família de quatro pessoas.

“O consumidor passará a gerar
energia, se livrando das tarifas de
energia, que subiram 50% em
2015”. A Aneel projeta crescimento
de 800% em 2016 na minigeração.

FOTOS: DIVULGAÇÃO

PLACAS FOTOVOLTAICAS em residência são tendência, já que, hoje, o investimento é recuperado em oito anos

geração à energia hidrelétrica, que
é limpa e renovável.”

Apesar da suspensão do leilão de
energias renováveis, empresários e
especialistas acreditam que a
energia solar vai continuar cres-
cendo a taxas elevadas no País nos

próximos anos.
O presidente da Empresa de

Pesquisa Energética, Luiz Barroso,
afirma que já vêm sendo adotados
vários tipos de estímulos para o
desenvolvimento da energia solar
no País. Ele cita o convênio com o

Conselho Nacional de Política Fa-
zendária (Confaz), que autoriza os
governos estaduais a isentarem de
ICMS a energia que é liberada pa-
ra a rede devido ao insumo obtido
com a minigeração distribuída.

ESPÍRITO SANTO
No Brasil, dos 26 estados e o Dis-

trito Federal, apenas cinco ainda
cobram Imposto Sobre Circulação
de Mercadorias e Serviços (ICMS)
sobre a geração de energia solar:
Santa Catarina, Paraná, Amazo-
nas, Amapá e Espírito Santo.

Até setembro de 2016, eram sete.
Cada vez mais consumidores se
interessam em investir na produ-
ção, mas desistem por não querer
pagar imposto sobre o sol.

Estudo publicado em 2013, o
Atlas Solar do Espírito Santo atesta
o potencial do território capixaba
para a geração de energia fotovol-
taica, mas, por enquanto, não há
um grande empreendimento do
gênero em operação no Estado.

R$ 125 bi
serão investidos até 2030

800%
cresceu a microgeração em 2016

OS NÚMEROS


