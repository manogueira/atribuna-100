
Co n c u r so s

VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 31

Abertas inscrições para
60 vagas de guarda-vidas
A Prefeitura de Aracruz
contrata profissionais,
com remuneração de
R$ 1.036, para atuar
nas praias do município
durante o verão
Jéssica Romanha

Quem quiser aproveitar a estação mais agitada e quente do ano para conseguir uma
oportunidade de emprego, mesmo
que temporária, no setor público,
pode entrar na disputa por uma
das 60 vagas abertas pela Prefeitu-
ra de Aracruz.

As chances são para candidatos
de nível fundamental completo
para o cargo de guarda-vidas. A re-
muneração dos contratados será
no valor de R$ 1.036,24, acrescido
de auxílio-alimentação e vale
t ra n s p o r t e.

As chances foram abertas por
meio de processo seletivo simplifi-
cado para atender às demandas da
Secretaria de Turismo e Cultura.

Para participar da seleção é exi-
gido que o candidato tenha, além
do nível fundamental completo,
nacionalidade brasileira, idade mí-
nima de 18 anos completos, que
esteja em dia com as obrigações
eleitorais e tenha certificado de
conclusão do Curso de Formação
de Guarda-Vidas, 2016, emitido
pelo Centro de Ensino e Instrução
de Bombeiros (Ceib) do Corpo de
Bombeiros Militar do Espírito
Sa n t o.

Os interessados podem se ins-
crever até a próxima quinta-feira

nas dependências da Secretaria de
Turismo e Cultura situada na rua
Zacarias Bento Nascimento, 167,
Aracruz. O horário de atendimen-
to vai das 9 horas ao meio-dia e das
14 horas às 18 horas.

Os profissionais serão lotados
por toda a orla do município e
também na Lagoa do Aguiar.

O processo seletivo será feito em
etapa única de avaliação, sendo
observadas as inscrições feitas ele-

tronicamente e as notas obtidas no
curso de formação de guarda-vi-
d a s.

Segundo a prefeitura, o número
de vagas abertas leva em conta o
contingente de munícipes e turis-
tas durante o verão, e também a
extensão da orla. Entre as funções
do guarda-vidas estão: realizar ta-
refas de vigilância e salvamento,
observando banhistas para preve-
nir afogamentos e salvar vidas;

orientar banhistas com animais na
praia e práticas esportivas.

Também é responsabilidade do
contratado realizar patrulhamen-
to marítimo com embarcação de
propulsão a motor, orientar ba-
nhistas, prestar informações ge-
rais e turísticas, participar de reu-
niões e elaborar relatório, respon-
sabilizar-se pelo controle e utiliza-
ção de equipamentos e materiais
colocados à sua disposição.

VINÍCIUS RANGEL — 02/11/2015

GUARDA-VIDA em ação: atuação será em praias e na Lagoa do Aguiar. Interessados podem se inscrever até quinta

Ifes contrata professor
substituto em 3 unidades

O Instituto Federal do Espírito
Santo (Ifes) publicou editais de se-
leção para professores substitutos
em diversas áreas nas unidades
Centro-Serrano, Ibatiba e Vitória.

Ao todo são 11 vagas, em 10
áreas distintas, e a remuneração
pode chegar a R$ 5.426,30, depen-
dendo da titulação do profissio-
nal, acrescidos de alguns benefí-
cios, como auxílio-alimentação,
auxílio-transporte e auxílio pré-
e s c o l a r.

No Campus Centro-Serrano há
uma vaga para a área de Le-
tras/Português e uma para a área
de Química. As inscrições come-
çam na próxima terça e vão até o
próximo dia 9. No Campus Ibatiba,
as oportunidades são para Enge-
nharia Florestal, Engenharia Am-
biental, Geografia, História, há
também uma vaga para cada área.
As inscrições já estão abertas e se-
guem até o dia 9.

Já no Campus Vitória, há cinco

vagas para as áreas de Letras/In-
glês, Eletrotécnica, Estradas, Se-
gurança do Trabalho e Sociologia.
Os interessados podem se inscre-
ver até o dia 20 deste mês.

Para todas as oportunidades, as

inscrições são feitas presencial-
mente na unidade que estão ofer-
tando as vagas. Os endereços po-
dem ser conferidos no site :
www.ifes.edu.br. Não haverá co-
brança de taxa de cadastro.

RODRIGO GAVINI — 03 / 1 0 / 2 0 1 6

IFES de Vitória é uma das unidades onde os aprovados irão atuar

Santa Leopoldina abre
cadastro de reserva

SAIBA MAIS

Processo seletivo
> PROCESSO SELETIVO: Prefeitura de

A ra c r u z .
> VAGAS: 6 0.
> CARGO: guarda- vidas.
> ESCOLARIDADE: nível fundamental.

Sa l á r i o
> SALÁRIO: de R$ 1.036,24.
> BENEFÍCIO: auxílio alimentação e va-

le transporte.

C a d a s t ro
> I NSCR IÇÃO: até a próxima quinta-

feira, nas dependências da Secreta-
ria de Turismo e Cultura situada na
rua Zacarias Bento Nascimento, 167,
Aracruz. O horário de atendimento
vai das 9 horas ao meio-dia e das 14
horas às 18 horas.

> TAXA: não há.

R equisitos
> REQUISITOS: idade mínima de 18

anos completos, estar em dia com as
obrigações eleitorais e ter certifica-
do de conclusão do Curso de Forma-
ção de Guarda-Vidas, 2016, emitido
pelo Centro de Ensino e Instrução de
Bombeiros (CEIB) do Corpo de Bom-
beiros Militar do Estado do Espírito
Sa n t o .

> SELEÇÃO: em etapa única de avalia-
ção de inscrição e notas obtidas no
curso de formação de guarda-vidas.

Fonte: Ed i ta l .

WILTON JUNIOR — 06/11/2013

BARRA do Sahy é uma das praias

A Prefeitura de Santa Leopoldi-
na, publicou edital para abertura
do processo seletivo simplificado
destinado a formação de cadastro
de reserva. As oportunidades são
para professor de educação infan-
til, professor de ensino fundamen-
tal – anos iniciais e professor de
ensino fundamental – anos finais.

A remuneração varia de R$

1.580,52 a R$ 2.370,75, de acordo a
titulação, para carga horária de 25
a 40 horas semanais.

As inscrições podem ser realiza-
das entre amanhã e quinta-feira de
forma presencial, na Secretaria de
Educação, situada na avenida Pre-
feito Hélio Rocha, n° 1110, Centro.
O atendimento vai das 8 horas às 11
horas e de 12h30 às 16 horas.


