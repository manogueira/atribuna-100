
Pol í t i ca

24 ATRIBUNA VITÓRIA, ES, SEGUNDA-FEIRA, 02 DE JANEIRO DE 2017

CENAS DA POSSE

Colaboração na gestão
A primeira-dama Nabila Furtado Oli-

veira, que acompanhou o prefeito Ju-
ninho em sua posse, contou que vai
trabalhar para ajudar o marido na ges-
tão da cidade.

“Vou continuar participando das
reuniões realizadas nos bairros. Des-
sa forma, posso ouvir os moradores e
levar as demandas para o Juninho,
deixando claro que não tenho cargo
nenhum na prefeitura”, explicou.

Bênção da mãe
A mãe do prefeito Geraldo Lu-

zia Júnior, o Juninho, Dona Dag-
mar Alves de Freitas, junto com
as filhas mais novas do prefeito,
Lara e Ana Júlia, acompanhou
atenta a posse do filho na tarde
de ontem, na Câmara dos Verea-
dores da cidade.

Ela que foi saudada e chama-
da de professora pelo filho, deu
sua bênção para o novo manda-
to do prefeito, que vai de 2017 a
2 0 2 0.

POSSES NA GRANDE VITÓRIA

Juninho vai adotar cartão
de ponto para os médicos
Prefeito explicou que
assim poderá controlar
serviços oferecidos em
Cariacica e anunciou
que unidades de saúde
serão informatizadas

Atrair novas empresas, abrir vagas de trabalho, qualificar os moradores da cidade,
proporcionar menos burocracia, e
principalmente, melhorar a saúde.
Esses foram os principais objeti-
vos abordados pelo prefeito Geral-
do Luzia Júnior (PPS), o Juninho,
empossado ontem, em seus pri-
meiros momentos da nova gestão
da Prefeitura de Cariacica.

Acompanhado do vice-prefeito,
Nilton Basílio Teixeira, ele disse
que um dos primeiros passos para
ajustar o serviço de saúde ofereci-
do pelo município será implantar
ponto eletrônico para os médicos.

“Vou começar implantando o
ponto eletrônico para os médicos
do município, dessa forma vou
conseguir controlar melhor os ser-
vidores, além de informatizar as
unidades de saúde”, afirmou.

O prefeito disse que suas ações
serão pautadas pela coerência,
além do senso de austeridade.

Ele contou que cortou diárias,
viagens de servidores e barrou au-
mento de gastos com aluguel. Ou-
tro corte será feito no número de
servidores comissionados, deixan-
do vagos 10% dos cargos.

Ele disse ainda que não vai reali-
zar concursos no município:

FOTOS: CLAUDIO POSTAY/DIVULGAÇÃO

“Vamos convocar candidatos
aprovados em concursos já reali-
zados, principalmente nas áreas
da saúde e da educação, mas não
vamos realizar novos concursos. O
que faremos será o processo seleti-
vo para contrato temporário.”

Ele disse que fez uma gestão
equilibrada, concluiu seu mandato
com as contas em dia e que é assim
que vai continuar sua nova gestão:

“Tenho um grande compromis-
so em melhorar a arrecadação da
cidade e criar uma boa ambiência
para os munícipes.”

Questionado sobre sua relação
com o PMDB do deputado esta-
dual Marcelo Santos — com quem
disputou a eleição, vencendo no
segundo turno — e com o PT do
deputado federal Helder Salomão
— seu antecessor à frente da pre-
feitura —, Juninho disse que espe-
ra ter um relacionamento com to-
dos os partidos e agentes políticos:

“Não vou negar relação, se for ne-
cessário para o bem da cidade, com
o deputado Marcelo Santos e o pró-
prio Helder Salomão, porém não
vou chamá-los para tomar café na
minha casa. Será a relação do pre-
feito com os deputados, e isso é si-
nal de maturidade para Cariacica”,
enfatizou.

Cesar Lucas derrota chapa
de Itamar Freire na Câmara

Na Câmara de Cariacica, o presi-
dente Cesar Lucas (PV) foi recon-
duzido ao cargo com 10 votos favo-
ráveis contra sete da chapa que ti-
nha Itamar Freire (PDT) à frente.
Houve ainda duas abstenções.

Reeleito para dois novos anos à
frente do Legislativo municipal,
Cesar buscou um discurso de inte-
gração, onde se dizia aberto a con-
versas com todos, inclusive os
membros da chapa de oposição.

“Eu tive uma conversa com todos
que estão aqui hoje (ontem), isso é
democracia, é um jogo de ideias. Eu
peço ajuda dos 18 vereadores aqui
presentes para fazer nossa Cariaci-
ca crescer e melhorar”, disse.

Durante a última semana, Ita-
mar Freire disse que manteria
oposição à Mesa Diretora durante
todo seu mandato, caso não fosse
eleito para o cargo.

Questionado sobre a declaração,
Cesar amenizou: “Não vou entrar
em polêmica com A ou B, mas vou
dizer a todos vereadores que a par-
tir de amanhã (hoje) o Cesar Lucas
está a disposição de todos”.

FÁBIO VICENTINI/AT

CESAR LUCAS comanda a Câmara

MESA DIRETORA

Cariacica
> PRESIDENTE: Cesar Lucas (PV)
> 1º VICE-PRESIDENTE: Celson An-

dreon (PT)
> 2º VICE-PRESIDENTE: Welington Sil-

va (PRTB)
> 1º SECRETÁRIO: André Lopes (PT)
> 2º SECRETÁRIO: Amarildo Araújo

( P M B)
> 3º SECRETÁRIO: Jorjão (PEN)

O QUE ELE DISSE SOBRE...

Promessa de novas creches
Redução de gastos

“Vou trabalhar para fazer uma ges-
tão equilibrada, mantendo as contas
em dia, assim como foi no meu man-
dato anterior.”

“Para isso, vou começar cortando
diárias e viagens de servidores, o que
inclui o prefeito, e também vou cortar
os gastos com aumento de aluguel.”

Concursos
”Não serão realizados novos con-

cursos no município, porém, os candi-
datos aprovados em concursos já rea-
lizados, principalmente, na área da
saúde e educação, serão convoca-
dos.”

“Vamos fazer processos s el e ti vo s
para contratar servidores temporá-

rios, em vez de contratar efetivos.”

E m p re e n d i m e n t o s
Juninho prometeu que vai trabalhar

para atrair novos empreendimentos
para Cariacica. Ele citou como exem-
plo a instalação da fábrica de garrafas
térmicas Alladin. O anúncio da insta-
lação da empresa na cidade será na
semana que vem.

O empreendimento vai abrir 150 va-
gas de trabalho direto.

“Para atrair novas empresas vamos
oferecer o que Cariacica tem de me-
lhor, que é a logística, por conta de sua
posição geográfica.”

“Vou cuidar dos empresários que
acreditaram na cidade.”

Cor tes
“já cortamos inicialmente 86 cargos

comissionados. Dessa vez deixarei va-
gos no mínimo 10% dos cargos comis-
sionados.”

Metas na Saúde
“Vamos Abrir a Unidade de Pronto-

Atendimento (UPA) de Flexal 2 e infor-
matizar todas as unidades de saúde do

município.”
“Também será instalado cartão ele-

trônico para os servidores da área da
saúde.”

“Vamos dar continuidade à Unidade
de Saúde de Nova Rosa da Penha 2.”

C re c h e s
“Já licitamos seis novas creches e

vamos avançar nisso, principalmente
porque é em parceria com o governo
f e d e ra l . ”

Gestão equilibrada
“Tenho um grande compromisso em

melhorar a arrecadação da cidade e
criar uma boa ambiência para os mu-
nícipes.”

“Vamos fazer p ro ce s so s seletivos para contratar servidores temporários, em vez de abrir concurso
para efetivos”

“Para atrair novas empresas vamos oferecer o que Cariacica tem de melhor, que é a logística, por conta de
sua posição geográfica”

“Não vou negar relação com o Marcelo Santos, mas não vou chamá-lo para tomar café na minha casa” Juninho, prefeito de Cariacica JUNINHO assinou o livro de posse, depois de descartar concurso público


