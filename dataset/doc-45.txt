
Ec o n o m i a
20 ATRIBUNA VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017

FALE COM A EDITORA ISABELA LAMEGO E-MAIL: economia@redetribuna.com.br

D ES E N VO LV I M E N TO

Investimentos milionários
que vão abrir empregos
Hospitais, portos, vias
públicas, aeroportos
e escolas estão entre
os 25 projetos que vão
criar 7 mil vagas no
Estado neste ano

Samantha Dias

No primeiro dia do ano, muita gente elabora projetos ou promete que aqueles que
ainda não saíram do papel serão
colocados em prática. Empresas,
prefeituras e o governo do Estado
também já têm planos para 2017 e
apontam os 25 investimentos mi-
lionários que vão abrir empregos
no Espírito Santo.

A reportagem de A Tribuna fez
um levantamento com informa-
ções das secretarias de Estado de
Desenvolvimento (Sedes); Trans-
portes e Obras Públicas (Setop);
Agricultura, Abastecimento, Aqui-
cultura e Pesca (Seag); e prefeitu-
ras de vários municípios para sa-
ber sobre os investimentos já co-
municados a cada órgão.

Na lista há, entre outras coisas, a
construção de escolas, hospitais,
estradas e portos; a continuação de
algumas obras; além de investi-
mentos em indústrias e empresas
que vão criar mais de 7 mil postos
de trabalho e ajudar o Espírito
Santo a crescer neste ano.

O secretário de Estado do De-
senvolvimento, José Eduardo
Azevedo, disse que as perspectivas
de investimentos são muito boas
não só para 2017 como também
para os próximos anos.

“Segundo projeção do Instituto
Jones dos Santos Neves, estão pre-
vistos R$ 57 bilhões até 2020 em
projetos, que devem criar 25 mil
empregos no Estado”, afirmou o

ANTONIO MOREIRA/AT

R$ 5 MILHÕES

Nova peça
A ampliação da HKM Partici-

pações, por meio da criação da
HKM Metais, é o projeto para
este ano de Bernardo Maximia-
no (à direita), que é sócio da
empresa.

O novo negócio, que tem pre-
visão de começar a operar em
março, vai fabricar anodos de
sacrifício, que é uma peça con-
tra a corrosão marinha, usada
em estruturas metálicas que
trabalham em contato com o
mar, segundo o diretor comer-
cial, Gerlyson Pegoretti, que
disse que o investimento é de
R$ 5 milhões.

“O momento de investir é
a g o ra ”, dizem economistas

Mesmo com perspectivas de di-
ficuldades neste ano, ou pelo me-
nos com incertezas quanto à reto-
mada da economia em 2017, eco-
nomistas garantem que o momen-
to é bom para investimentos.

“O momento de investir é agora.
Para as empresas que têm capital
para isso e querem assumir os ris-
cos, este é um bom momento”,
afirmou o economista Antonio
Marcus Machado.

Isso porque, segundo ele, em si-
tuações de crise algumas empre-
sas são excluídas, enquanto outras
c re s c e m .

“No Espírito Santo, o momento

para aportar recursos é ainda mais
oportuno, pois a tendência é que se
tenha mais condições de recupe-
ração que em outros estados, pelo
equilíbrio fiscal”, disse Machado.

O economista e coordenador-
geral da Faculdade Pio XII, Mar-
celo Loyola Fraga, concorda.

“Investir neste momento é assu-
mir um risco calculado. Quem sai
na frente conquista um nicho de
mercado e, além disso, mesmo que
os investimentos sejam feitos de
forma mais devagar, quando a eco-
nomia se recuperar é só acelerar o
processo. Não vai precisar come-
çar do zero”.

ADEMIR RIBEIRO — 2 5 / 03 / 2 0 1 5

ANTONIO MARCUS lembrou do equilíbrio fiscal do Espírito Santo

s e c re t á r i o.
Ainda de acordo com Azevedo, a

boa carteira de projetos, que inclui
infraestrutura, logística, energia,
petróleo e gás, vai dar mais com-
petitividade ao Espírito Santo,
além de iniciativas privadas, que
vão modernizar seus parques pro-
d u t i vo s.

O secretário de Estado de Obras,
Paulo Ruy Carnelli, acrescentou os
benefícios dos investimentos.

“Eles vão melhorar a mobilidade
metropolitana, atacar gargalos
existentes, abrir novas fronteiras
de desenvolvimento, além da cria-
ção de empregos diretos e indire-
tos”, disse.

As oportunidades de emprego
que serão criadas nas obras previs-
tas pela Setop e pelo Instituto de
Obras Públicas do Espírito Santo
(Iopes) serão, principalmente, pa-
ra engenheiro, operador de má-
quina, auxiliar de obras, pedreiro e
e l e t r i c i st a .

Na Serra, onde estão concentra-
dos muitos dos investimentos pre-
vistos, o prefeito Audifax Barcelos
disse que trabalha para criar novos
empregos e renda para a popula-
ção, atraindo empresas.

“Os empreendimentos terão im-
pacto maior na área social, onde
serão criados mais de 5 mil empre-
gos no decorrer do ano”, afirmou.

O P I N I Õ ES

“Mesmo com as dificuldades, o Estado tem boa carteira de investimentos” José Eduardo Azevedo, secretário
de Estado de Desenvolvimento

ANTONIO COSME - 11/05/2015

“As obras atacamos gargalos existentes ou abrem novas fronteiras” Paulo Ruy Carnelli, sec. de Estado
dos Transportes e Obras Públicas

LEONARDO DUARTE - 17/04/2015

“E m p re e n d i m e n to s terão impacto maior na área social, naqual serão criados mais de 5 mil empregos” Audifax Barcelos, prefeito da Serra

ANTONIO COSME - 11/11/2016


