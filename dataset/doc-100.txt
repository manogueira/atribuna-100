
Pol í t i ca

28 ATRIBUNA VITÓRIA, ES, SEGUNDA-FEIRA, 02 DE JANEIRO DE 2017

POSSES PELO PAÍS

Doria começa
t ra b a l h a n d o
como gari
Prefeito eleito de São
Paulo diz que estará
hoje, com secretários e
empresários, limpando
as ruas da região
central da cidade

SÃO PAULO

No primeiro dia no cargo de prefeito de São Paulo, João Doria (PSDB) prometeu
cuidado extra com a cidade, dando
início ao projeto de zeladoria do
município. Ontem, no Theatro
Municipal de São Paulo, durante a
posse, o tucano fez um discurso de
cinco minutos, repleto de acenos
aos vereadores paulistanos.

O novo prefeito de São Paulo
afirmou que hoje “antes do sol
raiar ”, estará vestido de gari junto
com sua equipe de secretários e al-
guns empresários para fazer uma
ação de limpeza na região central

de São Paulo, na operação que ele
batizou como “Cidade linda”.

“Estaremos vestidos de garis co-
mo gente simples que recebe seu
trabalho para dar demonstração
de humildade, igualdade e capaci-
dade de trabalho”, afirmou. Trata-
se de uma força-tarega que inclui
varrição, limpeza de bocas-de-lo-
bo, jardinagem, entre outros.

Doria listou 10 tópicos como ba-
ses de seu mandato. Entre os quais
“respeito ao Judiciário”, “resp eito
ao povo”, “respeito à transparên-
cia” e “respeito ao diálogo”.

“O prefeito estará aqui todo mês,
despachando na Câmara Munici-
pal”, disse Doria, reverberando uma
de suas promessas de campanha.

Ao anunciar a intenção de man-
ter harmonia com os legisladores
municipais, o prefeito citou o petis-
ta Eduardo Suplicy, vereador mais
votado na capital paulista e do Bra-
sil, e o tucano Mário Covas Neto,
com quem recentemente se indis-
pôs ao preteri-lo na disputa à presi-
dência da Câmara. Cabo eleitoral

de Doria, Milton Leite (DEM), vai
presidir a Casa por dois anos.

Assim como fez durante toda a
campanha eleitoral, que o levou à
vitória ainda no primeiro turno, fei-
to inédito em São Paulo, Doria vol-
tou a ressaltar que é um “gestor ”.

“Fomos eleitos defendendo a
gestão, e farei gestão na prefeitura,
respeitando os políticos. No Exe-
cutivo, serei um administrador”,
disse o prefeito, que pregou “res-

peito à eficiência e à inovação”.
Doria é o primeiro prefeito de São

Paulo eleito em primeiro turno des-
de 1992, quando as eleições passa-
ram a ter dois turnos. Ele assume o
cargo com R$ 2 bilhões em caixa,
deixados pelo ex-prefeito Fernando
Haddad (PT). A quantia representa
o saldo líquido que estará nas con-
tas da prefeitura, sem contar a verba
empenhada com restos a pagar, em
torno de R$ 4 bilhões.

MISTER SHADOW/AGÊNCIA ESTADO

“É proibido gastar”, avisa
Crivella em posse no Rio
RIO

“É proibido gastar”. Com essas
palavras de ordem, Marcelo Cri-
vella, do PRB, tomou posse ontem
como prefeito do Rio de Janeiro.
Na sessão realizada na Câmara,
também foram empossados os 51
vereadores eleitos – 18 deles nova-
tos na Casa –, além do vice-prefei-
to Fernando Mac Dowell (PR).

Crivella, de 59 anos, tentou pon-
tuar seu discurso com a promessa
de desempenhar uma administra-
ção austera com os gastos públicos
diante da crise financeira do Rio e
do País.

“O Brasil e o Rio estão em crise.
É tempo de cautela”, disse o novo
prefeito que, antes da posse, publi-
cou 78 decretos no Diário Oficial.

Crivella impôs como meta, em
edição extra do “Diário Oficial”, a
redução de 50% no gasto com car-
gos comissionados e de 25% nos

contratos em vigor. O novo prefeito
criou um grupo para identificar ca-
sos de supersalários no município.

“As chamadas mordomias são
uns dos símbolos mais execráveis
de abuso do poder público. Nesse
momento de grave crise, é preciso
enfrentar essa questão”, afirmou.

Crivella determinou também a
auditoria nos gastos dos últimos oi-
to meses de seu antecessor no car-
go, Eduardo Paes (PMDB). O obje-
tivo é avaliar se decisões adminis-
trativas tomadas pelo peemedebis-
ta geraram custos continuados para
a nova gestão. Ele ainda definiu co-
mo prioridade o combate a doenças
de verão, sobretudo chikungunya,
dengue e zika”, todas transmitidas
pelo mosquito Aedes aegypti.

Ligado à igreja Universal do Rei-
no de Deus, Crivella agradeceu aos
votos de “90% dos evangélicos” da
cidade e citou o tio, bispo Edir Ma-
cedo, líder da Igreja Universal.

FERNANDO FRAZÃO/ AGÊNCIA BRASIL

FERNANDO
MAC DOWEL,
vice-prefeito do
Rio, e o prefeito
eleito Marcelo
C r i ve l l a :
austeridade é
um dos lemas
da nova gestão

“Est a re m o s vestidos de gari como gente simples que recebe seu trabalho para dar demonstração de
humildade, igualdade e
capacidade de trabalho” João Doria, prefeito de São Paulo

REPRODUÇÃO DE INTERNET

MENOS DE 48H depois de sair da prisão, Rogério Lins
(PTN) tomou posse, ontem, como prefeito de Osasco
(SP) e, durante o discurso, chamou de injustiça as acu-
sações de que fez parte de esquema de contratação de
funcionários “fantasmas ” quando era vereador.

REPRODUÇÃO DE INTERNET

OUTRAS POSSES PELO BRASIL

REPRODUÇÃO DE INTERNET

EM SANTANA DO PIAUÍ (PI), o prefeito eleito Chico Borges
(PTB), de 42 anos, morreu ontem num acidente na rodo-
via PI-375, às 5 horas, 10 horas antes de sua posse. O
carro que dirigia bateu de frente com um ônibus.

RODRIGO FÉLIX LEAL/FUTURA PRESS

UM DIA após parar no hospital com crise de
ansiedade e falta de ar, Rafael Greca(PMN)
tomou posse como prefeito de Curitiba.

DORIA discursou por cinco minutos durante a sua posse, que foi repleta de acenos aos vereadores paulistanos

FORAGIDO, o prefeito eleito de
Embu das Artes (SP), Ney San-
tos (PRB), não tomou posse e
enviou uma carta para ser lida
durante a cerimônia. O vereador

Hugo Prado (PSB), foto, elei-
to presidente da Câmara,

assumiu em seu lugar.


