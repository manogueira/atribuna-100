
Espor tes
44 ATRIBUNA VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017

FALE COM O EDITOR FLÁVIO DIAS E-MAIL: esportes@redetribuna.com.br

Fla quita dívida com Romário
Foram anos de batalha judicial

por conta de uma dívida com ori-
gem em 1995, quando Romário foi
contratado pelo Flamengo após
ter sido eleito o melhor jogador do
mundo. Mas o clube rubro-negro
inicia 2017 sem mais nenhum dé-
bito com o ex-atacante.

O Fla fez um acordo com o tetra-
campeão mundial e quitou à vista
o que era devido em relação aos di-
reitos de imagem.

Romário ainda tinha a receber
cerca de R$ 18 milhões parcelados
até 2022. O valor era corrigido a
cada 12 meses. As últimas parcelas
mensais estavam em R$ 190 mil.

Para liquidar a fatura, as partes
decidiram trazer a valor presente a
dívida total. Sem as correções, o
atual senador ainda receberia em

torno de R$ 12 milhões do Fla-
m e n g o.

Durante as conversas, o Baixi-
nho deu 50% de desconto e o clube
rubro-negro quitou de uma só vez
cerca de R$ 6 milhões para colocar
fim no impasse de 20 anos.

Romário e Flamengo preferiram
encerrar a dívida do que prorrogá-
la por mais seis anos.

O clube se livrou de um incômo-
do processo, como fez recente-
mente com Ronaldinho, e o eterno
camisa 11 reforçou os cofres quan-
do julgou mais importante.

O Flamengo também quitou em
2015 uma dívida de R$ 4,2 milhões
com o ex-jogador.

O débito, neste caso, era por con-
ta de encargos não recolhidos en-
tre 1995 e 1999.

Fla e Romário não têm mais de-
bates na Justiça. Assim, a história
de 240 jogos e 204 gols está livre
dos incômodos processos.

A diretoria comandada pelo pre-
sidente Eduardo Bandeira de Mel-
lo comemorou bastante o fim do
impasse em conversas amistosas
com o Baixinho.

C O F R ES
Sem mais a receber do Flamen-

go, Romário ainda reforça os co-
fres com o rival Vasco. Só em 2016,
o senador recebeu quase R$ 4 mi-
lhões do cruz-maltino, panorama
que seguirá em 2017.

Apesar de não entrar mais em
campo, o eterno artilheiro segue
com o “salário mensal” de um re-
levante atleta profissional.

REPRODUÇÃO DE INTERNET

ROMÁRIO recebeu R$ 6 milhões do Flamengo e dívida teve ponto final

F L A M E NG O

Colheita de títulos no novo ano
Goleiro Muralha
relembra temporada
de ascensão e espera
conquistar, neste ano,
os troféus que não
vieram em 2016

GILVAN DE SOUZA/FLAMENGO

MURALHA só pensa em continuar se destacando com a camisa rubro-negra após cair nas graças da torcida

RIO

A torcida rubro-negra viveuuma montanha-russa deemoções em 2016. Do início
ruim, com eliminações na Copa
Sul-Minas-Rio, Campeonato Ca-
rioca e Copa do Brasil, o time fez
Brasileirão surpreendente e brigou
pelo título até as últimas rodadas,
terminado em terceiro.

O goleiro Alex Muralha seguiu o
mesmo ritmo do time e foi ga-
nhando espaço não só na equipe
titular, mas também no coração
dos torcedores.
O goleiro che-
gou ao clube, se
tornou titular e
ainda foi convo-
cado para a Se-
l e ç ã o.

Em entrevista
ao site “Lance!”,
o jogador come-
morou o grande
ano que viveu no Fla e faz planos
mais ousados ainda para 2017.

> Qual o balanço que você faz
da primeira temporada no Fla?

MURALHA – Em todos os clubes
que eu passei, sempre tive que tra-
balhar muito para conseguir uma
oportunidade e graças a Deus elas
sempre apareceram. Para goleiro é
mais difícil, é preciso estar sempre
pronto e preparado. Acho que meu
ano no Flamengo foi muito bom.

> Poderia ser melhor?
Sim, eu queria pelo menos um

título com a camisa do Flamengo,
deve ser uma coisa muito grandio-
sa, mas acho que estamos no cami-
nho. No ano que vem (2017) vamos
correr atrás do que faltou.

> Paciência é sua melhor vir-

tude para virar titular nos clu-
bes em que jogou?

Minha mãe costuma dizer que é
a teimosia. Paciência também, eu
sempre time muita. Mas teimosia
de querer e nunca desistir, mesmo
com muitos dizendo que não ia
dar certo. Tive treinadores que
chegaram para minha mãe e fala-
ram 'seu filho será jogador de ti-
mes medianos e não vai passar dis-
so'. E ela nunca me contou.

> Você ficou intimidado assim
que chegou ao Flamengo?

De início, assim que eu cheguei,
senti um pouco, sinceramente.
Mas com o tempo eu fui apren-
dendo, fui vendo que era capaz de
estar aqui, de fazer parte desse
grupo. E quando tive a oportuni-
dade de jogar fiz um bom jogo e
consegui dar uma sequência.

> Qual o balanço que você faz
da temporada do time?

Em geral, dá para dizer que o ba-
lanço foi satisfa-
tório. Claro que
a gente queria
um título, ser
campeão brasi-
leiro, mas por
tudo que acon-
teceu, todas as
c i rc u n st â n ci a s
contrárias que o
time teve du-

rante o ano, foi uma temporada
boa. Plantamos muitas coisas boas
em 2016 e tenho certeza que, em
2017, vamos colher muitas coisas
b o a s.

> Que aprendizado de 2016 fi-
cou para o Fla para 2017?

Nosso time foi o que mais viajou
em 2016, ia para lugares longe,
chegou a passar a semana inteira
longe de casa, mas faz parte da
nossa profissão. Acho que a grande
lição é que a gente pode se superar
independentemente das circuns-
tâncias que passa. Tenho certeza
que em 2017, a gente tendo um es-
tádio aqui no Rio já, isso vai nos
ajudar. Vamos ganhar títulos pelo
Flamengo em 2017, todos estão
com esse pensamento.

“Plantamos muitas coisas boas em 2016 e tenho certeza que vamos colher coisas boas em 2017” Muralha, goleiro do Flamengo


