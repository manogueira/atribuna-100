
VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 7

AT 2

PAULO OCTAVIO Vi t r i n e
po@redetribuna.com.br

* * *

CAR OL Coser está em São Mi-
guel dos Milagres, Alagoas, onde
passou a virada de ano ao lado de
a m i ga s .

* * *
E D UA R D O Campos curte dias
de relax em Cancún, no México,
com a filha e um grupo de ami-
gos.

* * *
O CIRURGIÃO Claudio Piras e
sua esposa, Jacqueline, passa-
ram o Réveillon em Bruxelas, on-
de se encontram com a filha Ste-
phanie. Eles seguem para um gi-
ro pela França com retorno pre-
visto para o próximo dia 12.

* * *

KAMIL A G ra ss i .

* * *
A TERAPEUTAGrazieli Marchi-
to começa o ano com novidades
no Norte do Estado. Ela passa
agora a atender pacientes no
município de Linhares, além de
Vitória e Vila Velha.

* * *
AL ESSA NDR A de Melo come-
morou a chegada de 2017 em Iriri,
ao lado do marido, Frederico Pe-
zenti, e da baby Elisa.

* * *
PAUL A Abdenor escolheu a
Praia da Bacutia para assistir aos
fogos da virada ao lado de ami-
gos e da família.

* * *
AN A Clara Benevides curte fé-
rias ao lado do amado, Felipe Ma-
zzei, em terras paradisíacas. O
casal já passou por Dubai e agora
está na Tailândia. Em seguida,
partem para o Vietnã.

* * *
O ESTILISTA Thiago Galavotti
aposta nas cangas com estam-
parias em 3D como grande ten-
dência deste verão.

Fe l i c i d a d e s ! ! !
Os parabéns da coluna aos aniversariantes da semana:

Munique Gomes e Elaine Dal Gobbo (hoje); Wilson Igreja
Campos (amanhã); Jéssica Polese, Fabrine Dias e Rosina
Botelho de Carvalho (quarta); Joel Miranda e Victor Oliveira
Pinheiro (quinta); Ricardo Silveira, Helen Quadrini Avanci-
ni, Suellen Decottignies e Sandra Kiefer (sexta) e Aline
Mantovanelli, Monique Tassar, Juliana Kwak, Fernando
Lisboa (sábado).

GUSTAVO FORATTINI/AT

GABRIELA Oliveira e Ana Eliza Assis em clima de verão

GUSTAVO FORATTINI/AT

Alô, fumacê!
Quem aproveita o verão para praticar atividades físicas

ou mesmo passear no calçadão da Praia da Costa sofre com
a presença de mosquitos no local.

Em tempos de zika vírus, dengue e chikungunya, a ordem
é investir na prevenção!

GUSTAVO FORATTINI/AT

FRED Reggiani e Guga Prates

GUSTAVO FORATTINI/AT

JULIANA P i re s

Um novo ano
2016 foi o ano das crises: eco-

nômica, ética, ambiental, políti-
ca e por aí vai. Porém, fecha-se
um ciclo e inicia-se um novo
com a esperança de um ano me-
l h o r.

E assim a coluna deseja um
Feliz 2017 cheio de paz, amor e
boas notícias aos leitores!

D I V U LG AÇ ÃO

FELIZ ANO Novo!!!! As belas Iara Quaresma e Francieli Crespo

* * *

Cicatrizes ao sol
Com a chegada da estação

mais quente do ano, é hora de
reforçar os cuidados com a pe-
le, sem esquecer de proteger as
c i c a t r i z e s.

Segundo a dermatologista
Juliana Drumond, quando no-
vas elas podem ficar escuras se
não forem protegidas. “O indi-
cado é aplicar o filtro solar ou
protegê-las com curativos”,
alerta.

T E R M Ô M ET RO

IN

OUT
P ESS I M I S M O

PENSAMENTO POSITIVO
PARA O NOVO ANO

GUSTAVO FORATTINI/AT

RHANDA R odrigues

Quem disse?
“Deixamos o Theo

assistir apenas 20
minutos de desenho
por dia.” Da cantora
Sa n d y.

Você sabia?
“As pessoas chatas vivem no in-

terior de uma câmara que só pro-
duz eco. Explore ideias, lugares e
opiniões.” Do livro “Como Ser Inte-
r e ss a n t e ”, de Jessica Hagy.

GUSTAVO FORATTINI/AT

DIANDRA Zipinotti e Bruno Felipe

* * *

COM A COLABORAÇÃO DE: Thama Boldrini


