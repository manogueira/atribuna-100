
Ciência e Tecnologia
VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 43

Homem de
Neander tal
era canibal,
aponta estudo

Os homens de Neandertal
que viviam nas cavernas de
Goyet, no que hoje é território
belga, não comiam apenas ca-
valos e renas para se alimentar,
mas também carne humana, in-
dica um novo estudo interna-
cional.

Isso fica demonstrado pelos
corpos despedaçados ou pelos
ossos fraturados para que a me-
dula fosse retirada encontrados
na região.

“É irrefutável, o canibalismo
era praticado aqui”, explica o
arqueólogo belga Christian
Casseyas enquanto percorre a
chamada terceira caverna de
Goyet, situada em um pequeno
vale perto das Ardenas belgas.

Os restos datam de cerca de
40 mil anos atrás, quando a pre-
sença na terra dos neandertais
estava chegando ao fim. Faltava
pouco para que dessem lugar ao
homem de Cro-Magnon, nosso
ancestral direto, com o qual ha-
viam coabitado.

Durante anos, os homens de
Neandertal, com um cérebro
um pouco maior que o do ho-
mem moderno, foram conside-
rados seres selvagens, apesar de
cuidarem dos corpos dos mor-
tos, como demonstram algumas
sepulturas da época.

Agora sabe-se que eles tam-
bém comiam seus pares. Já ha-
viam sido detectados casos de
canibalismo em populações de
neandertais estabelecidas na
Espanha (El Sidrón e Zafarraya)
e na França (Moula-Guercy e
Les Pradelles), mas nunca antes
num país do Norte da Europa.

CAV E R N AS
As cavernas de Goyet, ocupa-

das desde o Paleolítico, são gale-
rias de calcário de cerca de 250
metros de comprimento, escul-
pidas naturalmente pelo Sam-
son, um pequeno riacho que
atualmente está localizado a
poucos metros das cavernas.

O lugar começou a revelar
seus segredos em meados do sé-
culo XIX graças a um dos pre-
cursores da paleontologia,
Edouard Dupont (1841-1911),
que escavou cuidadosamente
várias cavernas, incluindo a de
Goyet, em 1867, onde encontrou
vários ossos e ferramentas.

HOMEM de Neandertal

Arte revela primeiros sinais
de Parkinson e Alzheimer
Comparações entre
pinturas de artistas
que desenvolveram
doenças neurológicas
apontam mudanças nos
padrões dos desenhos

SÃO PAULO

Uma análise detalhada das pinceladas de artistas que mais tarde desenvolveram
doenças neurológicas revelou in-
trigantes pistas sobre mudanças
cerebrais ocorridas anos antes do
surgimento dos sintomas óbvios
desses males.

O método matemático, que cha-
ma-se análise fractal, observa pa-
drões recorrentes presentes tanto
na Matemática como no mundo
natural. Padrões fractais podem ser
encontrados, por exemplo, em ár-
vores e nuvens. E também nas on-
das emitidas por nosso cérebro ou
nos nossos batimentos cardíacos.

O mesmo se aplica às pinceladas
de artistas, que podem ser compa-
radas à sua escrita. Como parte de
um estudo, a psicóloga Alex Forsy-
the, da Universidade de Liverpool,
na Inglaterra, realizou análises
fractais de mais de 2 mil pinturas
feitas por sete artistas famosos.

São eles: Pablo Picasso, Claude
Monet, Marc Chagall (que não ti-
nham diagnóstico de doenças
neurológicas), Salvador Dalí, Nor-
val Morrisseau (que desenvolve-
ram mal de Parkinson), Willem de
Kooning e James Brooks (diagnos-
ticados com mal de Alzheimer).

A pesquisadora encontrou mi-
núsculas variações nos padrões

observados nas pinceladas dos ar-
tistas. Mas, nos trabalhos daqueles
que mais tarde desenvolveram de-
mência ou mal de Parkinson os pa-
drões mudaram de forma peculiar.

“Verificamos que, até 20 anos an-
tes de serem diagnosticados com
algum transtorno neurológico, o
conteúdo fractal nas pinturas des-
ses artistas começou a diminuir.”

O artista de origem holandesa
Willem de Kooning, que morreu
em 1997 aos 93 anos, continuou a

pintar até sua oitava década de vi-
da, mesmo após ser diagnosticado
com Alzheimer. As pinceladas ob-
servadas nos primeiros quadros
do artista são diferentes das pre-
sentes em obras feitas mais tarde.

Mas no caso de artistas como
Monet e Picasso, que, pelo que se
sabe, morreram sem doenças neu-
rológicas, os padrões das pincela-
das mantiveram-se constantes.

Na obra de Picasso, um artista
que adotou estilos bastante dife-

rentes, a constância dos padrões
fractais verificados chamou parti-
cularmente a atenção.

EST U D O
O estudo não ajudará a diagnos-

ticar a demência ou outras doen-
ças neurológicas, os especialistas
explicam. O trabalho oferece, po-
rém, revelações importantes sobre
mudanças que podem estar ocor-
rendo no cérebro anos antes de
que uma doença apareça.

FOTOS: DIVULGAÇÃO

PINTURA pode apontar sinais no conteúdo 20 anos antes de doenças aparecerem, mas não vale como diagnóstico

Vem aí o alto-falante que
funciona debaixo d'água
LOS ANGELES, EUA

Após confirmar que apresenta-
rá novos smartphones durante a
feira de tecnologia CES 2017, a LG
anunciou que também apresenta-
rá uma nova caixa de som Blueto-
oth que, entre suas habilidades,
consegue flutuar.

A caixa PJ9 tem aparência futu-
rística e usa ímãs para flutuar so-
bre sua base. O alto-falante tam-
bém funciona debaixo d'água e, se-
gundo a fabricante, teria capacida-
de de ficar submerso por até 30
minutos sem sofrer qualquer tipo
de dano a uma profundidade de
até um metro.

Com uma única carga de bateria,
o PJ9 consegue autonomia para
funcionar até 10 horas. A LG diz
que quando a bateria acaba, a caixa

de som desce lentamente até tocar
sua base de apoio, que serve como
estação de carregamento.

A LG ainda não deu informa-
ções de preços ou de lançamento

A CAIXA DE
SIM PJ9
será lançada
durante a feira
de tecnologia
CES 2017
e é capaz de
flutuar e ficar
submersa
durante 30
minutos

para o alto-falante portátil, mas
dará mais detalhes sobre o dispo-
sitivo na próxima semana durante
o evento de tecnologia em Las Ve-
g a s.

Google cria
prêmio para
quem chegar
primeiro à Lua
MOUNTAIN VIEW, EUA

Em movimentações que pare-
cem o agito no grid de largada que
antecede as corridas de Fórmula 1,
diversas equipes espalhadas pelo
mundo se preparam para fazer em
2017 uma disputa inusitada: ganha
quem chegar primeiro à Lua.

Trata-se do Prêmio X Lunar Go-
ogle (GLXP), patrocinado pela gi-
gante da internet para fomentar o
desenvolvimento de tecnologias
espaciais de baixo custo e a explo-
ração do satélite natural da Terra.

A maior bolada – US$ 20 milhões
(R$ 65,1 milhões) – vai para o 1º
grupo que conseguir pousar na Lua
um módulo robótico capaz de se
deslocar por no mínimo 500 me-
tros pela superfície e transmitir ví-
deos e fotos em alta definição de lá.


