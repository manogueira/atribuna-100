
Política

VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 35

1 Luciano
Rezende
PPS
Vitória

QUEM ASSUME HOJE NAS CIDADES DO ESTADO

78 Prefeito
indefinido
Sem partido
Muqui

Eleitos tomam posse
Serão empossados hoje 76 prefeitos
 eleitos em todo o Estado.
Em Fundão, ainda é aguardada
uma decisão da Justiça Eleitoral
para validar ou não o registro do
mais votado. Já em Muqui, haverá
 nova eleição para prefeito.

2 Max
Filho
PSDB
Vila Velha

3 Audifax
Barcelos
Rede
Serra

4 Geraldo
Luzia “Juninho”
PPS
Cariacica

5 Edson
Magalhães
DEM
Guarapari

6 Gilson
Daniel
PV
Viana

7 Edélio
Guedes
PMDB
Afonso Cláudio

8 Paulo
Marcio Leite
DEM
Água D. do Norte

9 Brizola
Cor teletti
PSD
Águia Branca

10 José G.
Aguilar
PSDB
Alegre

11 Fernando
Lafayette
PSB
Alfredo Chaves

12 Luiz
Américo Borel
PMDB
Alto Rio Novo

13 Fabrício
Petri
PMDB
Anchieta

14 Fabricio
Thebaldi
PP
Apiacá

15 Jones
Cavaglieri
SD
Aracruz

16 Almir Lima
Barros
PSB
Atílio Vivácqua

17 Neto
Barros
PC do B
Baixo Guandu

18 Alencar
Marim
PT
B. São Francisco

19 Lauro
Vieira da Silva
PSDB
Boa Esperança

20 Marquinhos
Messias
PSD
Bom J. do Norte

21 João
Lourenço
PV
Brejetuba

22 Victor
Coelho
PSB
Cachoeiro

23 Luiz
Carlos Piassi
PMDB
Castelo

24 Sérgio
Meneguelli
PMDB
Colatina

25 Francisco
Ver vloet
PSDB
C. da Barra

26 Christiano
Spadetto
PMDB
C. do Castelo

27 Eleardo
Costa Brasil
PMDB
D. São Lourenço

28 Wanzete
Kruger
PSD
Domingos Martins

29 Ninho
PDT
Dores do Rio
Preto

30 Elias
Dal Col
PSD
Ecoporanga

31 Geraldo
Loss
PSDB
G. Lindenberg

32 Vera Lucia
Costa
PDT
Guaçuí

33 Luciano
Salgado
PMDB
Ibatiba

34 Duda
Zanotti
PMN
Ibiraçu

35 Reginaldo
de Souza
PMDB
Ibitirama

36 João
Paganini
PDT
Iconha

37 Carlos
Emerick Storck
PSDB
Irupi

38 Darly
Dettman
PMDB
Itaguaçu

39 Luciano
Paiva
PROS
Itapemirim

40 Ademar
Schneider
PRP
Itarana

41 Coronel
Weliton
PV
Iúna

42 R ogério
Feitani
PMN
Jaguaré

43 Sérgio
Fonseca
PSD
Jerônimo Monteiro

44 Otávio
Abreu Xavier
PP
João Neiva

45 Josafá
Storch
PMDB
Laranja da Terra

46 Guerino
Zanon
PMDB
Linhares

47 Herminio
Hespanhol
PMDB
Mantenópolis

48 Tininho
Batista
PP
Marataízes

49 Cacau
Lorenzoni
PMDB
Marechal Floriano

50 Geder
Camata
PSDB
Marilândia

51 Angelo
Guarçoni “Giló”
PMDB
Mimoso do Sul

52 Iracy
Baltar
DEM
Montanha

53 Osvaldo
Fernandes
PDT
Mucurici

54 Carlos
Bazzarella
PROS
Muniz Freire

55 Lubiana
Barrigueira
PSB
Nova Venécia

56 Sidiclei
de Andrade
PDT
Pancas

57 Bruno
Teófilo Araújo
PSDB
Pedro Canário

58 Arnobio
Pinheiro Silva
PRB
Pinheiros

59 Professor
Ricardo
PDT
Piúma

60 Sérgio
Murilo Coelho
PSDB
Ponto Belo

61 Amanda
Quinta Rangel
PSDB
P. Kennedy

62 Felismino
Ardizzon
PSB
Rio Bananal

63 Thiago
Fiorio
PROS
Rio Novo do Sul

64 Valdemar
Coutinho
PRP
Sta. Leopoldina

65 Hilário
Roepke
PMDB
S. Maria de Jetibá

66 Gilson
Amaro
DEM
Santa Teresa

67 Pedro
Dalmonte
PMN
São D. do Norte

68 Lucélia
Pim da Fonseca
SD
S. Gabriel da Palha

69 José C.
de Almeida
PMDB
S. José do Calçado

70 Daniel
da Açaí
PSDB
São Mateus

71 Rubens
Casotti
PMDB
S. Roque do Canaã

72 Alessandro
Torezani
PSDB
Sooretama

73 João
Altoé
PSDB
Vargem Alta

74 Braz
Delpupo
DEM
V.N. do Imigrante

75 Irineu
Wutke
SD
Vila Pavão

76 Robson
Parteli
PTN
Vila Valério

77 Prefeito
indefinido
Sem partido
Fundão


