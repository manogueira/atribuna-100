
Esportes

VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 47

CORRIDA DE SÃO SILVESTRE

Queniana campeã e recordista
Medalhista de ouro na
maratona na Olimpíada
do Rio, Jemima
Sumgong bate recorde
que durava cinco anos
e comemora o título

JEMIMA SUMGONG terminou
o trajeto de 15 km em 48min35,
marcando novo recorde feminino.
A marca anterior era de 48m48,
conquistada em 2011 pela também
queniana Priscah Jeptoo

SÃO PAULO

A queniana Jemima Sumgong fez do Brasil a sua casa em 2016. Após conquistar o ouro
 nos Jogos Olímpicos do Rio, a
maratonista de 32 anos foi a campeã
 da 92ª Corrida Internacional
de São Silvestre, disputada ontem
pela manhã em São Paulo (SP).

A atleta terminou o trajeto de 15
km em 48m35, marcando novo recorde
 feminino — o antigo melhor
tempo era de 48m48, da queniana
Priscah Jeptoo, em 2011.

A vitória coroa um ano vitorioso
da queniana. Antes mesmo de subir
 ao lugar mais alto do pódio na
Rio/2016, ela já havia conquistado
em abril a Maratona de Londres,
uma das mais tradicionais corridas
de rua do mundo.

Ontem, Jemima Sumgong liderou
 a prova de ponta a ponta.

Flomena Cheyech, do Quênia, ficou
 em segundo, com 49m14, Eunice
 Chumba (BAH), em terceiro,
com 50m24, Ymer Ayalew (ETI),
em quarto, com 51m40, e Ester Kakuri
 (QUE), em quinto, com 51m45.

A brasileira Tatiele
Carvalho terminou
em sétimo.

Num ano em que tudo
 deu certo, Jemima comemorou
 mais uma vitória,
 com direito a recorde.

“Estava muito quente e difícil,
 mas eu consegui imprimir
uma boa velocidade e estou muito
satisfeita com a vitória. Quero agradecer
 ao apoio dos brasileiros, que
foram muito acolhedores e me incentivaram
 o tempo inteiro”, afirmou
 a campeã olímpica no Rio e
agora vencedora da São Silvestre.

A brasileira melhor colocada foi
Tatiele Carvalho, em sétimo lugar.
Ela admitiu ter sentido por causa
do nível das concorrentes.

“Eu treinei muito para alcançar
um lugar no pódio, mas o nível estava
 altíssimo neste ano. Graças a
Deus, eu fui a melhor brasileira, o
que é uma vitória. Eu ainda vou
subir neste pódio e ainda vou ser
campeã da São Silvestre”.

Entre as mulheres, a última vitória
 brasileira aconteceu em 2006,
quando Lucélia Peres foi campeã.

Arrancada vencedora de etíope
Em uma chegada espetacular, o

etíope Leul Aleme conseguiu uma
arrancada para vencer a prova
masculina da São Silvestre ontem,
em São Paulo, com o tempo de

44min51, com vantagem de apenas
 dois segundos para o compatriota
 Dawit Admasu, na 92ª edição
 da tradicional prova disputada
no último dia do ano.

FELIPE RAU/AGÊNCIA ESTADO

LEUL ALEME venceu a prova de forma apertada em final emocionante

Giovani dos Santos fica em
quarto e faz homenagem

RESULTADOS

Pódio da prova masculina

“Eu vi que teria que correr muito
 nos últimos 300 metros para
vencer. Foi o que eu fiz. Estou muito
 feliz por ter vencido e por estar
aqui no Brasil. Eu gostei muito do
percurso e, principalmente, do carinho
 do público, que me apoiou e
aplaudiu durante toda a prova”,
disse o satifeito Aleme.

O melhor brasileiro na corrida
de 15 quilômetros foi Giovani dos
Santos, que terminou em quarto
com o tempo de 45m30, atrás do
queniano Stephen Kosgei, que terminou
 a prova em 45 minutos.

A hegemonia africana na São
Silvestre começou em 1992, quando
 o queniano Simon Chemwoyo
deu o primeiro título ao continente
 — sagrou-se bicampeão no ano
seguinte. De lá para cá, foram 19
vitórias de africanos contra seis
dos brasileiros.

Ontem, no começo da disputa, o
brasileiro Adriano de Oliveira chegou
 a liderar, alterando a ponta
com outro atleta do País, Reginaldo
 da Silva.

Giovani dos Santos também largou
 no pelotão de elite e conseguiu
se manter entre os primeiros colocados
 até o fim, perdendo força na
subida da avenina Brigadeiro, enquanto
 Reginaldo e Adriano diminuíra
 o ritmo bem antes, ficando
para trás.

Os metros finais reservaram
emoção, já três competidores africanos
 estavam muito próximos.
Mas Aleme levou a melhor.

A quarta posição na São Silvestre,
 ontem, deixou bastante satisfeito
 o mineiro Giovani dos Santos,
melhor brasileiro dos 15 km da
prova de rua paulistana.

O atleta repetiu o melhor resultado,
 obtido também nas edições
de 2012 e 2013, e afirmou que dedica
 o feito à Chapecoense, time
catarinense vítima de tragédia aérea
 na Colômbia há um mês. “O
quarto lugar foi uma conquista.

Quero dedicar para a Chapecoense.
 Não torcia para time algum,
mas agora vou torcer para eles. Espero
 que todos estejam lá em cima,
com Deus, e nos deem mais vitórias”,
 afirmou.

O capixaba Valério de Souza ficou
 no 10º lugar geral.

A última vitória brasileira na
prova masculina de São Silvestre
foi em 2010, com Marilson Gomes
dos Santos.

> 1º LUGAR: Leul Aleme, da Etiópia
(44m53)

> 2º LUGAR: Dawit Admasu, da Etiópia
(44m55)

> 3º LUGAR: Stephen Kosgei, do Quênia

 (45m)
> 4º LUGAR: Giovani dos Santos, do

Brasil (45m30)
> 5º LUGAR: Willian Kibor, do Quênia

(45m49)

ETER LEONE/AGÊNCIA ESTADO

GIOVANI DOS
SANTOS
recebe troféu
no pódio após
ficar na quarta
colocação da
Corrida de
São Silvestre

“Estava muito quente e difícil, mas eu consegui imprimir uma boa velocidade ” Jemima Sumgong, campeã

FELIPE RAU/AGÊNCIA ESTADO

Pódio da prova feminina
> 1º LUGAR: Jemima Jelagat Sumgong,

do Quênia (48m35)
> 2º LUGAR: Flomena Cheyech, do Quênia

 (49m15)
> 3º LUGAR: Eunice Cehbicii, do Bahrein

 (50m26)
> 4ª LUGAR: Ymer Wude, da Etiópia

(51m40)
> 5ª LUGAR: Ester Chesang Kakuri, do

Quênia (51m45)

RESULTADOS


