
VITÓRIA, ES, SEGUNDA-FEIRA , 02 DE JANEIRO DE 2017 FALE COM A EDITORA ALELVI CARNEIRO E-MAIL: at2@redetribuna.com.br

“Já estou
virgem de novo”,
diz Cristiana
Oliveira > 8

Shows
nacionais
agitam o
Estado > 5

Em “Passageiros ”, que
estreia quinta, os atores
Jennifer Lawrence e
Chris Pratt vão tentar
salvar 5 mil pessoas
adormecidas em nave

DIVULGAÇÃO

CURIOSIDADES

Cachês astronômicos
O orçamento do filme, cerca de US$

110 milhões, foi inflado pelos cachês
dos protagonistas. Jennifer Lawrence,
que pelo segundo ano consecutivo é a
atriz mais bem paga em Hollywood
(US$ 46 milhões), recebeu US$ 20 milhões,
 o dobro do colega, Chris Pratt.

Cenas de beijo
Sempre bem-humorada, Jennifer

 Lawrence disse que fica nervosa
 ao filmar cenas de beijo.
“Quando eu tive que beijar Josh
(Hutcherson) e Liam (Hemsworth)
 em ‘Jogos Vorazes’, eu pensava:
 ‘Quem liga?’, e comia mostarda
 e atum. Depois, eu fiz ‘Trapaça’
 com Christian Bale e deixava
 meus dentes sempre limpos

e comia bala de hortelã. Com
Pratt, eu definitivamente comia

balas de hortelã!”, brincou.

Bêbada na cama
Para gravar as cenas de sexo, a atriz

recorreu à bebida. “Fiquei realmente
muito bêbada. Mas depois, quando

cheguei em casa, isso me deixou mais
ansiosa e pensei: ‘O que eu fiz? Eu não
sei’”, contou aos risos.

Estrela carente
Perguntada sobre a química com

Chris Pratt na telona, Jennifer disse
que não foi preciso esforço. “Ele conseguiria
 ter química com um cacto”,
garantiu ela, que ficou amiga da esposa
 de Pratt, a também atriz Anna Faris.
“Acho que a mulher consegue perceber
 se você é o tipo de garota que roubaria
 o seu marido. Eu não passo essa
vibração. Minha vibração é mais no
estilo 'por favor, goste de mim' e isso
não é ameaçador”, contou.

Rude com fãs
Totalmente aversa às redes sociais e

superexposição, Lawrence diz que hoje
 é mais reservada e, algumas vezes,
até rude. “Comecei a ser muito rude e
mais reservada. As pessoas pensam
que já somos amigos porque sou famosa
 e eles sentem que me conhecem,
mas não os conheço. Eu tenho que me
proteger na minha bolha”, disse.

Suspense e romance
no espaço

Paula Dell'Isola

Mais de 5 mil pessoas emum sono profundo. Umsono que deveria durar
mais de um século até uma nova
vida, em um novo planeta. Exceto
para um homem. E ele não ficará
sozinho em pleno espaço em “Passageiros”,
 que estreia nos cinemas
na próxima quinta.

Na trama, dirigida por Morten
Tyldum (“O Jogo da Imitação”),
o engenheiro mecânico Jim Preston
 (Chris Pratt) está a bordo da
nave Avalon com milhares de
pessoas, em uma jornada de 120
anos até uma colônia distante da
Terra.

Todos os passageiros foram
induzidos a um estado de hibernação
 para suportar a duração
da viagem. Mas, por algum motivo
 desconhecido, Jim acorda

Cena assustadora
Em uma cena agonizante, Aurora

 (Jennifer Lawrence) se
afoga em uma piscina sem gravidade,
 e fica presa na água. Em
entrevistas, Jennifer Lawrence
comentou: “Gravar aquela cena
na piscina foi horrível. Foi uma
semana e meia dentro de uma
piscina 16 horas por dia. E dentro
de um tanque. Sendo virada de
cabeça para baixo, com água
entrando no meu nariz e bebendo
 água o dia inteiro. Foi horrível.
Mas, definitivamente valeu a pena”.


Inspirado em terror
Antes de despertar Aurora da

hibernação, a única companhia
de Jim (Pratt) é o barman robótico
 Arthur (Michael Sheen),
claramente inspirado no terror
“O Iluminado” (1980), com Jack
Nicholson. Programado para
servir os passageiros enquanto
ouve suas confissões no bar, ele
distribui conselhos e previsões,
mas nem sempre acerta.

Segredos
Chris Pratt disse que há muitos

segredos escondidos na trama:
“Por que esses dois acordaram
antes do tempo e a verdade por
trás de seu relacionamento são
os maiores mistérios do filme”.

O ENGENHEIRO MECÂNICO Jim Preston (Chris Pratt) e a jornalista Aurora Lane (Jennifer Lawrence) despertam 90 anos antes de fim de viagem espacial

DURANTE A JORNADA, o casal descobre problemas na nave Avalon

DIVULGAÇÃO

90 anos antes do prazo programado.
 E o pior, há problemas na
nave.

Percebendo que envelhecerá e
morrerá sozinho em pleno espaço,

Jim toma uma decisão controversa
e desperta a jornalista Aurora Lane
(Jennifer Lawrence). Juntos, eles
terão que salvar a adormecida tripulação
 da Avalon.

A mistura de romance, ação e
ficção científica aposta na química
entre dois dos maiores astros do
momento, Chris Pratt e Jennifer
Lawrence, para faturar.


