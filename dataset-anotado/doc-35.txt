
Cidades

8 ATRIBUNA VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017

N

OCEANO
ATLÂNTICO

ES

BA

MG

RJ

NORTE

SERRANA

GRANDE
VITÓRIA SUL

TEMPO E TEMPERATURA

FASES DA LUA AS MARÉS SOL

NASCENTE POENTE

CONDIÇÕES DAS PRAIAS

VITÓRIA

Na Grande Vitória
HOJE

0%
34º
23º

AMANHÃ

60%
34º
24º

64%
33º
24 º

ATÉ
QUARTA

CHEIA
DE 12/01
A 18/01

CRESCENTE
DE QUINTA
A 11/01

MINGUANTE
DE 19/01
A 26/01 6h05 19h25

TERÇA

MÁXIMA

MÍNIMA

MÁXIMA

MÍNIMA

MÁXIMA

MÍNIMA

PROBABILIDADE
DE CHUVA

PROBABILIDADE
DE CHUVA

PROBABILIDADE
DE CHUVA

PORTO DE VITÓRIA PORTO DE TUBARÃO

Fonte: Climatempo, Instituto Nacional de Meteorologia (Inmet), Marinha do Brasil, Observatório
Nacional e prefeituras de Vitória, Vila Velha e Serra.

Sol e nebulosidade
variável

Sol com nuvens e previsão de
4 mm de chuva com trovoadas

Sol com nuvens e previsão de
10 mm de chuva com trovoadas

ALTA
06h (1,4m)
17h56 (1,4m)

BAIXA
12h02 (0,4m)

ALTA
05h59 (1,4m)
17h51 (1,4 m)

BAIXA
12h01 (0,4 m)

SERRA

PRÓPRIA
LOCAL PONTO DE COLETA
Jacaraípe Em frente à col. de Férias
Jacaraípe Em frente à rua Caiçaras
Lagoa
Carapebus

Acesso principal, ao lado
da Colônia de Férias

Nova Almeida Acesso pela passarela,
em frente ao Bar da Orla

Nova Almeida Final do calçadão, em
frente à rua G. Becker

Praia de
Bicanga

Próximo ao início da
avenida Beira-Mar

Praia de
Carapebus

Em frente à Colônia de
Pescadores

Praia de
Carapebus

Em frente à rua
Projetada Vinte e Nove

Praia de
Manguinhos

Em frente ao restaurante
Primeira Estação

Praia de
Manguinhos

Em frente à rua Gov.
Francisco de Aguiar

Jacaraípe em frente à rua Marília
Resende Coutinho

Região Sudeste

Sol, nuvens
e chuva

Ensolarado

Parcialmente
nublado

Sol, nuvens,
chuva e trovoadas

LEGENDA

Chuvoso

No Estado

Sol e tempo seco no norte do RJ,
no ES e centro-norte e leste de
MG. No oeste e sul de SP chuva
isolada. No oeste e sul de MG, no
leste e norte de SP e no interior do
RJ sol com chuva isolada à tarde.

Cachoeiro de
Itapemirim

Santa Teresa
17º/29º

19º/31º18º/29º

21º/34º

22º/37º

Venda Nova

Guarapari

Domingos
Martins

Ecoporanga
20º/32º

São Mateus
22º/33º

Linhares
21º/33º

Colatina
21º/34º

Vitória 23º/34º

Passagem
do Transcol
mais cara a
partir de hoje

As tarifas dos ônibus do Transcol
estão 16,36% mais caras a partir de
hoje. Por ser domingo, os passageiros
 pagarão passagem com desconto,
 R$ 2,80, mas, amanhã, quem
pagava R$ 2,75 vai ter de desembolsar
 R$ 3,20, ou seja, R$ 0,45 a mais.

O economista e professor da
Multivix Antonio Marcus Machado
 explicou que, para quem ganha
salário mínimo, a nova tarifa vai
comprometer entre 10% e 15% do
orçamento.

A economista e professora da
Fucape Arilda Teixeira indicou
quem sentirá mais a mudança. “A
população com renda mais baixa é
que sentirá mais o reajuste.”

VANESSA DE OLIVEIRA

LOCAL PONTO DE COLETA
PRÓPRIA

LOCAL PONTO DE COLETA
Camburi Avenida Dante Michelini,

próx. ao viaduto Araceli
Cabrera Crespo 100m

Camburi D. Michelini esquina
com Silvino Grecco

Camburi D. Michelini, 100m à
direita do 3º píer

Camburi 100m à esquerda do 2º píer
Camburi esquina com avenida

Adalberto Simão Nader
Camburi esquina com a rua

Eugenílio Ramos
Santa Helena 200m à esquerda das

barracas da Curva da
Jurema

Ilha do Frade primeira praia à direita
Ilha do Frade Praia da Castanheira
Ilha do Boi Praia da Direita
Ilha do Boi Praia Grande
Santa Helena em frente às barracas da

Curva da Jurema

IMPRÓPRIA
Praia do
Canto

80m à direita do Iate
Clube

Camburi esquina com av. Nicolau
Von Schilgen

Camburi próximo à av. Carlos
Orlando Carvalho

Camburi 150m à esquerda do
Píer de Iemanjá

Santa Helena próximo à Escola de
Vela/ Praça dos Desejos

Ilha do Frade Praia da Ilha do Frade
Praia do Suá Praça do Papa

INTERDITADA
Camburi Canal de Camburi, ao lado

direito do Píer de Iemanjá
Praia do
Canto

80m à esquerda
da ponte da Ilha do Frade

Santa Helena 100m à direita da ponte
da Ilha do Frade

Praia do Meio embaixo da 3ª Ponte
Praia das
Castanheiras

Próx. ao antigo Bar do
Bigode

S. Antônio Praia de Santo Antônio
Canal de
Camburi

Ponte da Passagem
(J. da Penha)

NOVA

Proposta para análise
de praias em Vila Velha

Uma proposta para análise das
águas dos rios, lagoas e praias de
Vila Velha foi apresentada no último
 dia 22 ao município, após a
prefeitura realizar sessão de disputa
 de preços para contratação
de laboratório para executar a
análise da balneabilidade.

Segundo a prefeitura informou
em nota, de acordo com a legislação
 há um prazo de cinco dias
úteis para análise dos documentos,
 quando, então, o resultado final
 da licitação será divulgado por
meio do Diário Oficial. O município
está há mais de sete meses sem
divulgar as condições de balneabilidade.


RJ SP

MG
ES

A COLUNA FEZINHA não está sendo publicada, pois os sorteios foram realizados após o fechamento desta edição.

Supermercados
e lojas de
shoppings
fechados

Os supermercados da Grande
Vitória e as lojas de shopping não
funcionam hoje. A informação é
do superintendente da Associação
Capixaba dos Supermercados
(Acaps), Hélio Schneider.

Tanto as lojas quanto os quiosques
 dos shoppings vão permanecer
 fechados.

No entanto, quem quer aproveitar
 o dia de folga para comer, a praça
 de alimentação terá expediente
das 11 horas às 22 horas, exceto no
Shopping Norte Sul, onde a abertura
 é opcional.

Os cinemas também vão funcionar,
 conforme horários programados
 das sessões.

O elefante-marinho Fred,
que já apareceu várias vezes no
litoral capixaba, voltou ao Estado
 no último dia de 2016, desta
vez em uma praia da Serra.

Ele atraiu curiosos, que fotografaram
 o animal. Segundo o
Instituto de Pesquisa e Reabilitação
 de Animais Marinhos
(Ipram), ele está mais magro
que na última visita, no início
de 2016, e tem alguns ferimentos,
 mas está bem de saúde.

Fred não será retirado da
praia e continuará sendo monitorado
 pelo Ipram até decidir
voltar para o mar. O instituto
pede que a população não se
aproxime ou tente brincar com
ele para que possa descansar.

Para evitar aglomeração de
pessoas, o Ipram pediu para
não divulgar o nome da praia.

Expedito Garcia
vai ter corredor
exclusivo
para ônibus

A Prefeitura de Cariacica esclarece
 que o trecho da avenida Expedito
 Garcia, entre a rotatória da
rua Bolívar de Abreu e a praça de
Campo Grande, não será transformado
 em mão única, conforme
publicado na página 14 da edição
de ontem de A Tribuna.

A via continuará com dois sentidos.
 Porém, no sentido Terminal
Campo Grande, haverá um corredor
 exclusivo de ônibus. Os carros
deverão trafegar pela rua Getúlio
Vargas, a partir da Emef Stélida
Dias. No sentido contrário, o tráfego
 será liberado para carros e ônibus.
 A previsão da faixa exclusiva é
no primeiro semestre de 2017.

Elefante-marinho
Fred está de volta


