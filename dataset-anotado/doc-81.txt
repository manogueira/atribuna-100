
Cidades

VITÓRIA, ES, SEGUNDA-FEIRA, 02 DE JANEIRO DE 2017 ATRIBUNA 9

Bruna e Neymar
assumem namoro
Durante a virada do
ano, craque usou as
redes sociais para
publicar uma foto em
que aparece ganhando
um beijinho da atriz

RIO

Neymar e Bruna Marquezine resolveram assumir o relacionamento, na noite do
Réveillon. Os indícios de que o jogador
 e atriz estavam juntos de novo
 começaram nos últimos meses,
mas eles estavam evitando tocar
no assunto.

Nos últimos dias, eles foram flagrados
 juntos, mas não haviam falado
 publicamente sobre o romance.
 Porém, durante a virada do ano,
o craque usou as redes sociais para
publicar uma imagem, em seu Instagram,
 em que aparece ganhando
um beijinho da atriz.

Na legenda, ele desejou um Feliz
2017 e ainda colocou um coração.

Os fãs causaram o maior alvoroço,
 já que estavam torcendo para
que o casal “Brumar ” (mistura de
Bruna e Neymar) reatasse o namoro.
 Muitos deles chegaram a repostar
 as imagens feitas por Neymar
 nas redes.

Em agosto de 2014, Bruna confirmou
 que havia terminado o relacionamento
 de cerca de dois
anos com o jogador de futebol. Nos
últimos meses, no entanto, a atriz
vivia viajando para Barcelona para
se encontrar com o namorado.

No último dia 21, os dois estiveram
 na festa à fantasia em comemoração
 ao aniversário do surfista
Gabriel Medina. Neymar publicou
uma foto em suas redes sociais ao
lado de Bruna, ele vestido de Batman
 e ela de Mulher-Gato.

NEYMAR E BRUNA na imagem publicada pelo jogador em seu Instagram

Primeiro bebê do ano
nasceu em Cariacica

O primeiro bebê do ano no Estado
 é um menino que nasceu de
parto normal na Maternidade de
Cariacica, à 1h25, pesando quase
cinco quilos.

Com o pequeno nos braços, a
mãe do bebê, a doméstica Jaqueline
 Romeiro da Silva, de 28 anos,
contou que ainda não escolheu o
nome do filho. “Gosto de Mateus
e minha mãe prefere Elizeu. Ainda
 estou em dúvida”, explicou.

Jaqueline começou a sentir as
dores do parto por volta das 17h de
sábado, achou que poderia esperar
 para ir ao médico, mas as dores
ficaram mais fortes e ela procurou
a maternidade. “era 1h25 quando
ele nasceu”, recordou.

O pequeno chega ao mundo já
tendo quatro irmãos, dois meninos
 e duas meninas. E a mãe já
pensa em vê-lo indo para a escolinha.

 “Imagino ele estudando,
assim como os outros”, afirmou
Jaqueline.

O bebê pesa 4,495 quilos e mede
 54 centímetros. O diretor clinico
 e técnico da maternidade de
Cariacica, Eduardo Pereira, pensou
 que ele não fosse o primeiro
do ano. “Geralmente, o primeiro
bebê do ano nasce perto de meia-noite,
 por isso imaginei que em
outra maternidade tinha havido
parto nesse intervalo”.

Ele também falou da importância
 do bebê ter nascido de parto
normal, tanto para a saúde dele
quanto para a da mãe. “A paciente
deve sempre priorizar o parto
normal. E caso haja necessidade
de fazer cesariana, que ela seja
feita durante o trabalho de parto.
O ideal é que o corpo sinalize que
é hora do neném nascer”.

LEONARDO BICALHO/AT

JAQUELINE DA SILVA, 28 anos, com o menino, que nasceu com 4,49 kg

Passageiros têm de
fazer ceia em avião
MANAUS

O ano de 2017 não começou como
 o planejado para cerca de 300
passageiros do voo 914 da America
Airlines, que deveria ter deixado o
Rio rumo a Nova Iorque às 23 horas
 da última sexta-feira. Os planos
de um Réveillon nos Estados Unidos
 foram frustrados e os passageiros
 acabaram fazendo a ceia
dentro de um avião em Manaus.

Pior, foram desembarcados depois,
 não receberam as malas e
somente hoje deverão conseguir
completar a viagem, segundo a
previsão feita pela companhia
aérea. Os transtornos começaram
 quando todos já estavam
embarcados, na noite de sexta.

Eles tiveram de deixar a aeronave
 devido a um problema técnico.
 O avião só decolou do Aeroporto
 do Galeão no dia seguinte,
no sábado, às 13 horas.

Ao sobrevoar Roraima, depois de
quatro horas de voo, uma passageira
 sentiu-se mal e o piloto decidiu
retornar para Manaus (AM). Ela foi
retirada por médicos, mas exigiu
que devolvessem sua bagagem. Esse
 procedimento teria durado quase
 quatro horas e a ceia de Ano Novo
 teria sido servida na aeronave.

O voo acabou suspenso em seguida,
 pois os tripulantes já teriam
estourado a carga horária máxima
permitida. A decisão da American
Airlines foi levar todos para o Ceasar
 Business, em Manaus.

FOTOS: INSTAGRAM

A apresentadora Angélica também
 compartilhou ontem uma foto
 em que aparece ao lado do marido,
 Luciano Huck, e outros amigos,
 mostrando Bruna e Neymar
abraçados.

Além da amada, Neymar recebeu
 muitos famosos para a virada

de ano em sua nova mansão, localizada
 no condomínio Portobello,
na cidade de Mangaratiba, no litoral
 do Rio de Janeiro. De férias no
Brasil, ele está curtindo pela primeira
 vez a mansão, que comprou
em outubro por R$ 28 milhões na
cidade vizinha a Angra dos Reis.

Espetáculo de cores no céu
durante a virada em Camburi

A praia de Camburi, em Vitória, ficou lotada
para as boas-vindas a 2017, e um espetáculo de
cores e luzes preencheu o céu na virada do ano.
Muitos aguardaram a chegada no novo ano em
tendas na areia ou até mesmo da avenida Dante
 Michelini, que ficou interditada. Foram sete
minutos de fogos e muita emoção.

A Banda Trilha iniciou a festa, seguida pelo
grupo Feijão Balanço e Neno Bahia. A prefeitura
estimava um público de 200 mil pessoas, mas a
Polícia Militar não divulgou o número oficial.

DIEGO ALVES/PMV


