
Esportes

VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017 ATRIBUNA 45

MERCADO DA BOLA

Última chance de
mostrar o que sabe
Na lista de jogadores
que renderam abaixo
do esperado no Vasco,
Junior Dutra conta
com o Carioca para
provar o seu valor

RIO

Junior Dutra chegou ao Vasco no meio do ano passado, apedido do técnico Jorginho,
que encheu a bola do meia-atacante.
 Porém, foram apenas 13 partidas
 e um único gol.

O rendimento abaixo do esperado
 colocou o nome do jogador na
lista de possíveis dispensas. No entanto,
 Dutra acredita que ainda
tem uma última chance de provar
o seu valor. Com contrato até o fim
do próximo Campeonato Carioca,
ele espera aproveitar a competição
para se firmar em São Januário.

“Tenho contrato até o fim do
Campeonato Carioca e ainda não
conversei com a diretoria sobre renovação.
 Vou definir meu futuro
quando me reapresentar”.

A reapresentação do elenco do
Vasco após as férias acontece amanhã
 à tarde, em São Januário. De
volta à Série A do Brasileiro, o time
vai conviver com a pressão de buscar
 melhores resultados.

Apesar do pouco que mostrou
em campo em 2016 e das críticas
que o time recebeu na reta final da
Segundona do Brasileiro, Junior
Dutra faz uma avaliação positiva
do seu retorno ao País.

Revelado pelo Santos, ele ficou
sete anos fora do Brasil.

“Foi um ano muito intenso, de
experiências maravilhosas. Conseguimos
 o acesso com muitas coisas
 que não vivia mais no futebol,
como algumas dificuldades e pressão
 da torcida, mas tivemos um final
 de temporada feliz. Realmente,
voltei para o Brasil (risos)”.

Até agora, o Vasco anunciou
apenas o meia argentino Escudero
como reforço para a temporada.

PAULO FERNANDES/VASCO — 06/09/2016

JUNIOR DUTRA tem contrato com o Vasco até o fim do próximo Carioca

Flu pega pesado na faxina
RIO

O Fluminense segue no processo
 de faxina do elenco, se livrando
dos jogadores que não serão aproveitados.
 Depois de Magno Alves e
Cícero, o próximo que pode ter seu
futuro definido é o lateral-direito
Wellington Silva.

O jogador tem negociações
avançadas com o Bahia e a tendência
 é a de que ele troque de clube

Wellington Silva tem contrato
com o tricolor das Laranjeiras até
o fim de 2018, mas após sofrer críticas
 da torcida, principalmente no
segundo semestre de 2016, entrou
na lista dos que estão fora dos planos
 para a nova temporada.

A tendência é a de que o jogador
seja emprestado para os baianos,
com o Flu se comprometendo a
pagar parte dos salários do lateral-direito
 — o time carioca pagaria R$
40 mil dos R$ 180 mil que Wellington
 Silva recebe mensalmente.

Outro lateral que também deve
ser negociado é William Matheus.
O canhoto tem conversas avançadas
 para defender o Coritiba. Ainda

 no setor, o Fluminense também
tenta se desfazer de Giovanni, outro
 que, a princípio, não será aproveitado
 pelo técnico Abel Braga.

Léo Pelé, cria de Xerém e que estava
 emprestado ao Londrina, retorna
 às Laranjeiras a pedido do
novo treinador.

Até o momento, o Flu contratou
somente os equatorianos Sornoza
e Orejuela, que se destacaram no
Independiente del Valle/EQU na
última Libertadores.

O clube ainda tenta a contratação
 do atacante argentino Barcos,
ex-Palmeiras e Grêmio e que está
de saída do Vélez/ARG.

Porém, o jogador também entrou
 nos planos do Santos e do
Atlético Nacional/COL.

13 jogos
Junior Dutra fez pelo Vasco

1 gol
o meia-atacante marcou

OS NÚMEROS

DESTAQUES NA TV

TV GAZETA
> 10H00 — Esporte Espetacular
ESPN BRASIL
> 11H30 — Campeonato Inglês: Watford

 x Tottenham
> 14H00 — Campeonato Inglês: Arsenal

 x Crystal Palace
ESPN
> 16H00 — NFL: Miami Dolphins x New

England Patriots
> 19H30 — NFL: Washington Redskins

x New York Giants
ESPN+
> 19H30 — NFL: Denver Broncos x Oakland

 Raiders

Obs.: Mudanças na programação são de responsabilidade
 das emissoras.

DANIEL AUGUSTO/ AGÊNCIA CORINTHIANS

ATACANTE
LUCCA está
sem espaço no
Corinthians e
deve deixar o time

EXCEPCIONALMENTE, a coluna Gilmar Ferreira não foi publicada na
edição de hoje.

Lucca interessa ao Fogão
Reserva importante no título

brasileiro de 2015 e titular durante
o primeiro semestre de 2016, Lucca
 perdeu espaço e pode deixar o
Corinthians. Nos últimos dias, o
clube recebeu consultas do Botafogo
 sobre as possibilidades de adquirir
 o atacante para 2017.

Sem Neilton, que foi repassado
pelo Cruzeiro — que o havia emprestado
 ao Botafogo — ao São
Paulo, Lucca seria reposição para o
elenco do treinador Jair Ventura.

Se era imprescindível há pouco
mais de um semestre, quando foi
comprado do Criciúma, o atacante
hoje é uma peça da qual o Corinthians
 considera abrir mão.

Titular da ponta esquerda de Tite

 antes da ida do treinador para a
Seleção, Lucca foi comprado por
R$ 5 milhões do Criciúma. A intenção
 do Corinthians é recuperar
parte do investimento feito na
compra de 60% dos direitos econômicos
 do jogador. Informado
sobre a situação do atacante, o Botafogo
 agora avalia possibilidades.

Uma delas é também investir em
um jogador que interessa ao Corinthians:
 Willian Pottker, atacante
 da Ponte Preta que foi um dos
artilheiros do último Brasileirão
com 14 gols. A pedida da Ponte,
porém, assusta interessados: R$ 10
milhões pela multa rescisória.

Osvaldo, do Fluminense, também
 é visto como opção.


