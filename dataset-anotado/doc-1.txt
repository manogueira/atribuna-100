
VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017

SINAIS DE VITÓRIA

FALE COM A EDITORA ALELVI CARNEIRO E-MAIL: at2@redetribuna.com.br

Você é um
vencedor ?

FERNANDO RIBEIRO/AT

Matheus e
Kauan em
clima de
praia > 3

Poesias
hipnotizantes
de Carlos
Nejar > 5

Psicólogos afirmam
que pequenos
compor tamentos
no dia a dia revelam
se a pessoa é
realmente vitoriosa

Kariny Baldan

Subir ao topo de um pódio ou ganhar uma medalha até representam uma conquista,
mas nem sempre traduz o que é
uma pessoa vencedora.

Para psicólogos, não adianta obter
 sucesso profissional se você carece
 de bons relacionamentos, por
exemplo. O verdadeiro vencedor
tem as diferentes área da vida funcionando
 em harmonia e está satisfeito
 consigo próprio.

No livro “Florescer ”, o criador da
psicologia positiva, Martin Seligman,
 fala sobre os pilares do bem-estar:
 as emoções positivas vividas,
o engajamento com metas, os relacionamentos,
 o significado e o propósito
 de vida, e as conquistas.

A fundadora da Associação de
Psicologia Positiva da América Latina,
 Daniela Levy, explica que um
vencedor mantém esses pilares em
equilíbrio. “Estímulos externos focados
 em coisas materiais trazem
satisfação temporária. A duradoura
vem das experiências e vivências.”

Contudo, há quem tenha sucesso
 mas não se enxergue como vencedor.
 A psicóloga e coach em saúde

 e bem-estar Sharon Feder diz
que, ao longo da vida, as pessoas
são treinadas para observar o lado
negativo. “Se o indivíduo mudar
essa percepção, reconhecerá o que
tem”, afirma ao AT2 .

A orientação de Sharon é praticar
 exercícios diários, como listar
suas forças e ressaltar os bons
acontecimentos. Afinal, um vencedor
 valoriza o que há de bom.

A vitória pode vir de diferentes
formas, de acordo com o que cada
pessoa anseia. Para uns, é a família
unida. Para outros, a conclusão de
um curso. O ponto em comum a
todos é a satisfação pessoal.

Segundo a especialista em desenvolvimento
 pessoal Taty Nascimento,
 “apenas
a própria pessoa
 pode
mensurar se
é vencedora,
já que ela conhece
 seus
propósitos ”.

Auto avaliação
Muitas pessoas ignoram uma série

 de sinais de que são bem-sucedidos.
 Se você se reconhece abaixo, é
um vencedor.

AMOR PRÓPRIO E AO PRÓXIMO
> FESTEJA vitórias de seus colegas.
> QUANDO recebe um elogio, não fica

constrangido, pois sabe seu valor.
> VOCÊ NÃO está gordo(a). Apenas gosta

de comer. Mas está de olho na saúde.
> NÃO LIGA para o que dizem. Sua roupa

 é seu reflexo.
> CONSEGUE facilmente lembrar de

cinco pessoas que o amam.
> COM FREQUÊNCIA, diz “te amo” para

eles.
> SÓ FALA que ama se for verdade.

 melhor sua raiva.
> É, se ganhasse mais, seria bom. Mas

isso não é a prioridade. Está feliz.
> SABE aceitar os defeitos dos outros.
> FICOU (realmente!) alegre ao ver

o(a) ex feliz ou com outra pessoa.


METAS
> AO FALHAR em prova ou trabalho,

não desanima, busca melhorar.
> POSSUI metas realistas a cumprir.

XÔ, COISAS RUINS!
> RECONHECE as más pessoas e se

distancia .
> NÃO VIVEse lamentando pelos males

das coisas: simplesmente ignora.
> RECORDA-SE perfeitamente da última

 vez que disse “não”.

RELAÇÃO COM O OUTRO
> PEDE AJUDA quando precisa. Afinal,

sabe ser humilde.
> É CAPAZ de se pôr no lugar do outro.

E evita sofrimento desnecessário.
> É DOMINGO? Não sofre com a solidão.

 Tem com quem bater um papo.

RECONHECIMENTO
> QUANDO CHEGA em casa, suspira:

“Que bom, meu lar!”
> SUA CASA foi decorada do seu jeito.
> ÀS VEZES, se surpreende ao olhar o

espelho: como está bonito!
> RECONHECE que é bom trabalhador.

EXIGÊNCIA
> JÁ NÃO se culpa por uma frustração.

Sabe que haverá outra chance.
> CADA VEZ faz menos dramas e controla

“Sinto-me realizada”
Para a pedagoga Thaiz Araújo,

26 anos, a vitória veio com a combinação
 de diferentes fatores.

“Ainda tenho que conquistar
muitas coisas, mas já tenho uma família
 linda, um esposo dedicado,
um casamento feliz, minha casa

própria e, recentemente, consegui
o diploma que sempre sonhei. Sinto-me
 realizada”, afirma.

Thaiz conta que nem sempre foi
fácil. “A faculdade foi uma fase difícil,
 tendo que conciliar estudo,
trabalho e casamento, mas foi uma
meta que tracei e consegui atingir.”

“Estou bem comigo”
Aos 25 anos, a empresária Paula Moura

afirma: “Sou muito vencedora!”
O desenvolvimento do próprio negócio é uma

das principais conquistas da jovem, mas não faltam
 motivos para se sentir vitoriosa. “Construí minha
 loja, comprei meu carro, faço tudo por conta
própria, sou independente, estou terminando minha
 faculdade...”

Mas não só a vida profissional está bem. Paula
também está satisfeita com seus relacionamentos
 familiar e amoroso, e com a saúde em dia. “Tenho
 uma vida saudável. Busco sempre melhorar,
mas estou bem comigo”, completa.

Fonte: Especialistas em comportamento e “El País”

FERNANDO RIBEIRO/AT


