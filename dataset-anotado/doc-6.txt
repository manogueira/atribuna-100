
6 ATRIBUNA VITÓRIA, ES, DOMINGO, 01 DE JANEIRO DE 2017

AT2

Diretas
Striptiras ILAERTE

Níquel Náusea I FERNANDO GONSALES

Chiclete com Banana I ANGELI

Quadrinhos

Horóscopo
Áries
21/03 a 20/04

REGENTE: MARTE - Diz o ditado
que quando uma porta se fecha,
 outra melhor se abre. É
possível que algum de seus caminhos
 esteja se fechando.
Encare os desafios como oportunidade
 de transformação.

Touro
21/04 a 20/05

REGENTE : VÊNUS - A perseverança
 com que você trabalha para
realizar os seus objetivos pode
estar sendo desafiada por
ideias inovadoras. Se for o caso,
 talvez o melhor seja aceitar
o desafio.

Gêmeos
21/05 a 20/06

REGENTE: MERCÚRIO - Quando a
falta de confiança toma conta,
não permite que você supere
as barreiras que surgirem. Por
isso, o melhor a fazer é resolver
os conflitos internos que o impedem
 de acreditar em si.

Câncer
21/06 a 22/07

REGENTE: LUA - O ambiente familiar
 tende a deixar você mais
seguro para agir de forma mais
livre. Inclusive, você deve se
sentir mais à vontade para expressar
 as oscilações de humor.


Leão
23/07 a 22/08

REGENTE: SOL - No convívio com
as pessoas o respeito é imprescindível
 para que se possam
 negociar as diferenças.
Tudo as ações influenciam de
alguma forma na vida do outro
e vice-versa.

Virgem
23/08 a 22/09

REGENTE: MERCÚRIO - Para que o
entusiasmo inicial se mantenha
 ao longo de seus relacionamentos,
 é importante evitar
que se acomodem, e sempre
buscar novidades para sair da
rotina.

Libra
23/09 a 22/10

REGENTE: VÊNUS - Mesmo quando
 as provocações parecem
passar dos limites, é difícil que
você perca o juízo. No entanto,
ao ponderar excessivamente e
pelo que o outro vai pensar pode
 dificultar as relações.

Escorpião
23/10 a 21/11

REGENTE: PLUTÃO - Cada um sente
 as emoções ao seu modo e a
necessidade de que o outro
sinta tudo de forma recíproca
pode afastar. Afinal, dificilmente
 o outro sente da mesma
forma que você.

Sagitário
22/11 a 21/12

REGENTE : JÚPITER - Quando a
vontade de aprender é intensa,
nada deve deter você no seu
desenvolvimento pessoal. Se
você conseguir escutar a sua
intuição, ela pode estar agindo
a seu favor.

Capricórnio
22/12 a 20/01

REGENTE: SATURNO - Ao perceber
que a forma com que você se
organizava não está obtendo
os resultados esperados, é
preciso mudar. Aproveite as
transformações como forma
de ajustar suas estratégias.

Aquário
21/01 a 19/02

REGENTE: URANO - A grande
maioria de suas ansiedades
pode ser eliminada se você fizer
 acordo com o tempo. Nesse
acordo implica não correr atrás
e nem deixar que ele corra
atrás de você.

Peixes
20/02 a 20/03

REGENTE : NETUNO - As relações
afetivas se alimentam de trocas
 emocionais. Ao agir de modo
 afetuoso, você está contribuindo
 para tornar as suas relações
 mais prazerosas e equilibradas.


PUBLICAÇÃO






 AUTORIZADA 










POR 


REVISTAS





 COQUETEL








Palavras Cruzadas Diretas
WWW.COQUETEL.COM.BR © REVISTAS COQUETEL 2016

3 Gerações I RENNER

Labirinto Gêmeos
APENAS DOIS DESENHOS SÃO
IGUAIS. DESCUBRA QUAIS.

Resposta: 1 e 4

AJUDE A ALICE A
CHEGAR ATÉ AS FRUTAS.


