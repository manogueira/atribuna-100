# atribuna-100

Dataset com 100 documentos do jornal local do estado do Espirito Santo A Tribuna.

Este dataset é uma amostra de alguns erros encontrados na extração de texto por meio da leitura de metadados. Ele possui os textos originais e corridos.

No total existem 14703 termos diferentes.